# Xero Sync

This module facilitates the automated synchronisation of Drupal entities and
Xero accounting items.

Xero is a cloud accounting SAAS. It's API allows developers to create and
update accounting items like invoices, payments, contacts, etc. A Drupal API
for accessing the Xero API is provided by Drupal's Xero API module.

This module Xero Sync uses Xero API to help developers keep specific Drupal
entities synchronised with their counterparts on Xero. Some examples of
possible use cases include:

* Syncing some or all Drupal users with Xero contacts
* Syncing some or all Drupal Commerce products with Xero items
* Syncing a Drupal vocabulary with a Xero tracking category

This module is primarily an API module that provides an API for developers
to easily implement for their own use cases.
It provides APIs that help with:

* Identifying an existing Xero item that corresponds to a certain Drupal
entity
* Creating a Xero item based on a Drupal entity
* Updating a Xero item when its corresponding Drupal entity is updated
* Storing a reference to a corresponding Xero item on a Drupal entity
* Queue management, so communications with Xero's API can happen on cron runs

Full details of how to use the module are provided in the official
documentation: https://www.drupal.org/docs/8/modules/xero-sync

The included submodule Xero Sync User Contact synchronises Drupal users with
Xero contacts, identifying corresponding contacts by their email address. It
serves as a demonstration of how to use Xero Sync to initiate and customise
the synchronisation of a Drupal entity type and a Xero item type.

Currently all communication is initiated from Drupal to Xero; but it is
conceptually in scope for this module to help with one day with  responding
to web hooks coming from Xero to Drupal.
