<?php

namespace Drupal\Tests\xero_sync\Unit;

use Drupal\xero_sync\XeroSyncItemFinder as XeroSyncItemFinder;
use Drupal\Core\Entity\EntityInterface;

/**
 * A version of XeroSyncItemFinder that is easier to unit test.
 *
 * Many protected methods are redeclared as public.
 *
 * @see plugin_api
 */
class TestItemFinder extends XeroSyncItemFinder {

  /**
   * The discovered plugin definitions.
   *
   * @var array
   */
  protected $definitions;

  /**
   * Instantiated plugins.
   *
   * @var array
   */
  protected $instances;

  /**
   * Constructs a new XeroSyncItemFinder object.
   *
   * @param array $definitions
   *   The plugin definitions found by getDefinitions();.
   * @param array $instances
   *   An array of mock plugins.
   * @param array $cache
   *   (optional) A cache.
   */
  public function __construct(array $definitions, array $instances, array $cache = []) {
    $this->definitions = $definitions;
    $this->instances = $instances;
    self::$cache = $cache;
  }

  /**
   * {@inheritDoc}
   */
  public function getDefinitions() {
    return $this->definitions;
  }

  /**
   * {@inheritDoc}
   */
  public function createInstance($plugin_id, array $configuration = []) {
    return $this->instances[$plugin_id];
  }

  /**
   * {@inheritDoc}
   */
  public function getItemFromDefinitions(array $definitions, EntityInterface $entity) {
    return parent::getItemFromDefinitions($definitions, $entity);
  }

  /**
   * {@inheritDoc}
   */
  public function getItem($entity, $create = FALSE) {
    return parent::getItem($entity, $create);
  }

  /**
   * {@inheritDoc}
   */
  public function getMatchingDefinitions($entity, $create = FALSE) {
    return parent::getMatchingDefinitions($entity, $create);
  }

  /**
   * {@inheritDoc}
   */
  public function getItemFromCache($type, $id, $create) {
    return parent::getItemFromCache($type, $id, $create);
  }

}
