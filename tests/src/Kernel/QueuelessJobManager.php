<?php

namespace Drupal\Tests\xero_sync\Kernel;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\TypedData\TypedDataManagerInterface;
use Drupal\xero_sync\XeroSyncEntityHandlerInterface;
use Drupal\xero_sync\XeroSyncJobManager;
use Drupal\xero_sync\XeroSyncJobManagerInterface;
use Drupal\xero_sync\XeroSyncQueueJobTrait;
use Drupal\xero_sync\XeroSyncSynchronizerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * A job manager class that bypasses the queue, facilitating testing.
 *
 * It covers both sides of the queue, putting items in and processing them out.
 */
class QueuelessJobManager extends XeroSyncJobManager {

  // By using the queue job trait, we replicate what happens when
  // queue jobs are processed, without having to deal with the actual queues.
  use XeroSyncQueueJobTrait;

  /**
   * Construct a queueless job manager object.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type manager.
   * @param \Drupal\xero_sync\XeroSyncSynchronizerInterface $synchronizer
   *   The synchronizer.
   * @param \Drupal\Core\TypedData\TypedDataManagerInterface $typed_data_manager
   *   The typed data manager.
   * @param \Drupal\xero_sync\XeroSyncJobManagerInterface $job_manager
   *   The job manager.
   * @param \Symfony\Component\Serializer\SerializerInterface $serializer
   *   The serializer.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\xero_sync\XeroSyncEntityHandlerInterface $entity_handler
   *   The entity handler.
   */
  public function __construct(LoggerInterface $logger, EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info, XeroSyncSynchronizerInterface $synchronizer, TypedDataManagerInterface $typed_data_manager, XeroSyncJobManagerInterface $job_manager, SerializerInterface $serializer, TimeInterface $time, XeroSyncEntityHandlerInterface $entity_handler) {
    $this->setProperties($logger, $entity_type_manager, $entity_type_bundle_info, $synchronizer, $typed_data_manager, $job_manager, $serializer, $time, $entity_handler);
  }

  /**
   * {@inheritdoc}
   */
  protected function enqueue($queueId, array $jobData) {
    // We override the parent enqueue() method, so that instead of
    // adding an job to the queue, we call the queue processing logic
    // that would normally handle that job when the queue was processed.
    $this->doProcessItem($jobData, $queueId);
  }

}
