<?php

namespace Drupal\Tests\xero_sync\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\entity_test\Entity\EntityTest;

/**
 * Tests the job manager service.
 *
 * @group xero_sync
 */
class QueuingTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'user',
    'entity_test',
    'serialization',
    'xero',
    'xero_sync',
  ];

  /**
   * A test entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * The entity id.
   *
   * @var int
   */
  protected $entityId = 23;

  /**
   * The entity type.
   *
   * @var string
   */
  protected $entityType = 'entity_test';

  /**
   * The job manager.
   *
   * @var \Drupal\xero_sync\XeroSyncJobManager
   */
  protected $jobManager;

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('user');
    $this->installEntitySchema('entity_test');
    $entity = EntityTest::create([
      'id' => $this->entityId,
    ]);
    $entity->save();
    $this->entity = $entity;

    $this->jobManager = \Drupal::service('xero_sync.job_manager');

  }

  /**
   * Test the sync queue with create and update.
   */
  public function testQueueEntitySyncDefault() {
    $this->jobManager->queueEntitySync($this->entity);
    $data = [
      'entity_id' => $this->entityId,
      'entity_type' => $this->entityType,
      'entity_bundle' => $this->entity->bundle(),
      'create' => TRUE,
      'update' => TRUE,
    ];
    $this->assertQueueItem('xero_sync_sync', $data);
  }

  /**
   * Test the sync queue with update but not create.
   */
  public function testQueueEntitySyncNoCreate() {
    $this->jobManager->queueEntitySync($this->entity, FALSE);
    $data = [
      'entity_id' => $this->entityId,
      'entity_type' => $this->entityType,
      'entity_bundle' => $this->entityType,
      'create' => FALSE,
      'update' => TRUE,
    ];
    $this->assertQueueItem('xero_sync_sync', $data);
  }

  /**
   * Test the sync queue with create but not update.
   */
  public function testQueueEntitySyncNoUpdate() {
    $this->jobManager->queueEntitySync($this->entity, TRUE, FALSE);
    $data = [
      'entity_id' => $this->entityId,
      'entity_type' => $this->entityType,
      'entity_bundle' => $this->entityType,
      'create' => TRUE,
      'update' => FALSE,
    ];
    $this->assertQueueItem('xero_sync_sync', $data);
  }

  /**
   * Test the sync queue without create or update.
   */
  public function testQueueEntitySyncNeither() {
    $this->jobManager->queueEntitySync($this->entity, FALSE, FALSE);
    $data = [
      'entity_id' => $this->entityId,
      'entity_type' => $this->entityType,
      'entity_bundle' => $this->entityType,
      'create' => FALSE,
      'update' => FALSE,
    ];
    $this->assertQueueItem('xero_sync_sync', $data);
  }

  /**
   * Test the unsync queue when the item is not specified.
   */
  public function testQueueEntityUnsync() {
    $this->jobManager->queueEntityUnsync($this->entity);
    $data = [
      'entity_id' => $this->entityId,
      'entity_type' => $this->entityType,
      'entity_bundle' => $this->entityType,
      'item_guid' => NULL,
      'item_type' => NULL,
    ];
    $this->assertQueueItem('xero_sync_unsync', $data);
  }

  /**
   * Test the unsync queue when the item is specified.
   */
  public function testQueueEntityUnsyncWithItemDetails() {
    $this->jobManager->queueEntityUnsync($this->entity, 'test_item_type', 'test_item_guid');
    $data = [
      'entity_id' => $this->entityId,
      'entity_type' => $this->entityType,
      'entity_bundle' => 'entity_test',
      'item_guid' => 'test_item_guid',
      'item_type' => 'test_item_type',
    ];
    $this->assertQueueItem('xero_sync_unsync', $data);
  }

  /**
   * Test the unsync queue when the entity needs serializing.
   */
  public function testQueueEntityUnsyncSerialize() {
    $this->jobManager->queueEntityUnsync($this->entity, NULL, NULL, TRUE);
    $data = [
      'entity_id' => $this->entityId,
      'entity_type' => $this->entityType,
      'entity_bundle' => 'entity_test',
      'item_guid' => NULL,
      'item_type' => NULL,
      'entity' => \Drupal::service('serializer')->serialize($this->entity, 'json'),
    ];
    $this->assertQueueItem('xero_sync_unsync', $data);
  }

  /**
   * Assert a job is in the queue.
   *
   * @param string $queueId
   *   The queue id.
   * @param array $data
   *   The job data.
   */
  protected function assertQueueItem($queueId, array $data) {
    $queue = \Drupal::queue($queueId);
    $this->assertEquals(1, $queue->numberOfItems());
    $item = $queue->claimItem();
    foreach ($data as $key => $value) {
      $this->assertArrayHasKey($key, $item->data);
      $this->assertEquals($value, $item->data[$key]);
    }
    $this->assertEquals(count($data), count($item->data));
  }

}
