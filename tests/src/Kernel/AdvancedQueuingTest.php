<?php

namespace Drupal\Tests\xero_sync\Kernel;

use Drupal\advancedqueue\Entity\Queue as AdvancedQueue;
use Drupal\advancedqueue\Job;

/**
 * Tests the job manager service when the advanced queue module is installed.
 *
 * @group xero_sync
 */
class AdvancedQueuingTest extends QueuingTest {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'user',
    'entity_test',
    'serialization',
    'xero',
    'xero_sync',
    'advancedqueue',
  ];

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installSchema('advancedqueue', ['advancedqueue']);
  }

  /**
   * Assert a job is present in an advanced queue.
   *
   * @param string $queueId
   *   The queue id.
   * @param array $data
   *   The job data.
   */
  protected function assertQueueItem($queueId, array $data) {
    // Nothing in the ordinary queue.
    $queue = \Drupal::queue($queueId);
    $this->assertEquals(0, $queue->numberOfItems());

    $advancedQueue = AdvancedQueue::load($queueId);
    $backend = $advancedQueue->getBackend();
    $jobCounts = $backend->countJobs();
    $this->assertEquals([Job::STATE_QUEUED => 1], array_filter($jobCounts));
    $payload = $backend->claimJob()->getPayload();
    foreach ($data as $key => $value) {
      $this->assertEquals($payload[$key], $value);
    }
    $this->assertEquals(count($data), count($payload));
  }

}
