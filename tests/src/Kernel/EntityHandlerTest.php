<?php

namespace Drupal\Tests\xero_sync\Kernel;

use Drupal\entity_test\Entity\EntityTest;

/**
 * Tests the entity handler service.
 *
 * @group xero_sync
 */
class EntityHandlerTest extends XeroSyncTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'user',
    'serialization',
    'xero',
    'xero_sync',
  ];

  /**
   * The field name.
   *
   * @var string
   */
  protected $fieldName = 'xero_sync';

  /**
   * The entity handler.
   *
   * @var \Drupal\xero_sync\XeroSyncEntityHandler
   */
  protected $entityHandler;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('entity_test');
    $this->entityHandler = \Drupal::service('xero_sync.entity_handler');
  }

  /**
   * Test what happens on handleInsert.
   */
  public function testHandleInsert() {
    $this->setUpXeroField('entity_test');
    $entity = EntityTest::create([]);
    $entity->save();
    $this->entityHandler->handleInsert($entity);
    $this->assertEntitiesQueued([$entity], 'xero_sync_sync');
  }

  /**
   * Test what happens on handleUpdate.
   */
  public function testHandleUpdate() {
    $this->setUpXeroField('entity_test');
    $testGuid = $this->createGuid(FALSE);
    $entity = $this->createEntityWithContact($testGuid);
    $entity->original = $entity;
    $this->entityHandler->handleUpdate($entity);
    // Nothing changed, so no need to sync.
    $this->assertEntitiesQueued([], 'xero_sync_sync');
  }

  /**
   * Test what happens on handleUpdate if $entity->original is not set.
   */
  public function testHandleUpdateWithoutOriginal() {
    $this->setUpXeroField('entity_test');
    $testGuid = $this->createGuid(FALSE);
    $entity = $this->createEntityWithContact($testGuid);
    $this->entityHandler->handleUpdate($entity);
    $this->assertEntitiesQueued([$entity], 'xero_sync_sync');
  }

  /**
   * Test what happens on handleDelete.
   */
  public function testHandleDelete() {
    $this->setUpXeroField('entity_test');
    $testGuid = $this->createGuid(FALSE);
    $entity = $this->createEntityWithContact($testGuid);
    $this->entityHandler->handleDelete($entity);
    $this->assertEntitiesQueued([$entity], 'xero_sync_unsync');
  }

  /**
   * Test syncIfAppropriate().
   */
  public function testSyncIfAppropriate() {
    $this->setUpXeroField('entity_test');
    $entity = EntityTest::create([]);
    $entity->save();
    $this->entityHandler->handleUpdate($entity);
    $this->assertEntitiesQueued([$entity], 'xero_sync_sync');
  }

  /**
   * Test syncMultipleIfAppropriate().
   */
  public function testSyncMultipleIfAppropriate() {
    $entity1 = EntityTest::create([]);
    $entity1->save();
    $entity2 = EntityTest::create([]);
    $entity2->save();
    $this->entityHandler->syncMultipleIfAppropriate('entity_test');
    $this->assertEntitiesQueued([$entity1, $entity2], 'xero_sync_sync');
  }

  /**
   * Assert that entities have been queued by Xero Sync.
   *
   * @param array $entities
   *   The entities.
   * @param string $queueId
   *   The id of the Xero Sync queue.
   */
  protected function assertEntitiesQueued(array $entities, $queueId) {
    $queue = \Drupal::queue($queueId);
    $this->assertEquals(count($entities), $queue->numberOfItems());
    foreach ($entities as $entity) {
      $item = $queue->claimItem();
      $this->assertEquals($entity->id(), $item->data['entity_id']);
    }
  }

}
