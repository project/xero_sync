<?php

namespace Drupal\Tests\xero_sync\Kernel;

use Drupal\xero_sync\XeroSyncUtilitiesTrait;

/**
 * Tests the utilities trait.
 *
 * @group xero_sync
 */
class UtilitiesTest extends XeroSyncTestBase {

  /**
   * {@inheritDoc}
   */
  protected static $modules = ['serialization', 'xero'];

  use XeroSyncUtilitiesTrait;

  /**
   * The typed data manager.
   *
   * @var \Drupal\Core\TypedData\TypedDataManagerInterface
   */
  protected $typedDataManager;

  /**
   * {@inheritDoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->typedDataManager = \Drupal::service('typed_data_manager');
  }

  /**
   * Test the buildXeroItemWithId() method from XeroSyncUtilitiesTrait.
   */
  public function testbuildXeroItemWithId() {
    $testGuid = $this->createGuid(FALSE);
    $testType = 'xero_contact';
    $item = $this->buildXeroItemWithId($testType, $testGuid);
    $this->assertEquals('xero_contact', $item->getPluginId());
    $this->assertEquals($testGuid, $item->get('ContactID')->getString());
    $this->assertTrue(isset($item->stub));
    $this->assertTrue($item->stub);
  }

}
