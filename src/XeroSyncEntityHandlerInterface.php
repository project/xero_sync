<?php

namespace Drupal\xero_sync;

use Drupal\Core\Entity\EntityInterface;

/**
 * An interface for a class that handles sync interaction with entities.
 *
 * It handles syncing entities with Xero items when when entities are created
 * or updated. This is typically invoked from entity insert and update hooks.
 *
 * @package Drupal\xero_sync
 */
interface XeroSyncEntityHandlerInterface {

  /**
   * Respond to a Drupal entity being created.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   An entity.
   */
  public function handleInsert(EntityInterface $entity);

  /**
   * Respond to a Drupal entity being updated.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   An entity.
   */
  public function handleUpdate(EntityInterface $entity);

  /**
   * Respond to a Drupal entity being deleted.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   An entity.
   */
  public function handleDelete(EntityInterface $entity);

  /**
   * Gets the name of an entity type's field that stores its Xero id.
   *
   * The returned value does not guarantee that the field actually exists,
   * it should always be checked with $entity->hasField().
   *
   * @param string $entityType
   *   The machine name of the entity type.
   * @param string $bundle
   *   (optional) The machine name of the entity bundle.
   *
   * @return string
   *   The machine name of the field.
   */
  public function getFieldName($entityType, $bundle = NULL);

  /**
   * Determine whether syncing is appropriate for a entity.
   *
   * This method exists to allow inheriting classes an easy way to restrict
   * syncing based on the properties of an entity; e.g. to sync only nodes
   * of a particular type, or users with a particular role.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return array
   *   An array of boolean flags, keyed by context, indicating appropriateness.
   */
  public function getAppropriatenessFlags(EntityInterface $entity);

  /**
   * Initiate syncing for an entity if appropriate.
   *
   * If $create or $update are unspecified, their values will be provided by
   * calling the XeroSyncAppropriateEvent.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param bool|null $create
   *   (optional) Whether to create a Xero item if no matching one exists.
   * @param bool|null $update
   *   (optional) Whether to update the matched Xero item if it exists.
   *
   * @see \Drupal\xero_sync\XeroSyncEntityHandlerInterface::getAppropriatenessFlags()
   */
  public function syncIfAppropriate(EntityInterface $entity, $create = NULL, $update = NULL);

  /**
   * Initiate unsyncing for an entity if appropriate.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param string $itemType
   *   (optional) The data type of the Xero item.
   * @param string $itemId
   *   (optional) The id of the Xero item.
   * @param bool $serialize_entity
   *   (optional) Whether to serialize the entities, e.g. if entity is deleted.
   */
  public function unsyncIfAppropriate(EntityInterface $entity, $itemType = NULL, $itemId = NULL, $serialize_entity = FALSE);

  /**
   * Initiate syncing for a set of entities.
   *
   * If $create or $update are unspecified, their values will be provided by
   * calling the XeroSyncAppropriateEvent.
   *
   * @param string $entity_type
   *   The machine name of the entity type.
   * @param array|null $entity_ids
   *   (optional) An array of entity ids.
   * @param bool|null $create
   *   (optional) Whether to create a Xero item if no matching one exists.
   * @param bool|null $update
   *   (optional) Whether to update the matched Xero item if it exists.
   */
  public function syncMultipleIfAppropriate($entity_type, $entity_ids = NULL, $create = NULL, $update = NULL);

  /**
   * Initiate unsyncing for a set of entities.
   *
   * @param string $entity_type
   *   The machine name of the entity type.
   * @param array|null $entity_ids
   *   (optional) An array of entity ids.
   * @param bool $serialize_entity
   *   (optional) Whether to serialize the entities, e.g. if entity is deleted.
   */
  public function unsyncMultipleIfAppropriate($entity_type, $entity_ids = NULL, $serialize_entity = FALSE);

}
