<?php

namespace Drupal\xero_sync\Event;

/**
 * The names of events used by the Xero Sync module.
 */
final class XeroSyncEvents {

  /**
   * The name of the event fired when preparing a Xero item based on an entity.
   *
   * This always has the entity type appended to it, e.g.
   * "xero_sync.sync_to_item_from_entity.user".
   *
   * This event allows you to customize the item before it is posted to Xero.
   * The event listener method receives a
   * \Drupal\xero_sync\XeroSyncPropertiesEvent instance.
   *
   * When modifying the item, the listener should use the event's
   * ::setModified() method, so that the fact that the modification has
   * happened is flagged and can be responded to properly.
   *
   * @Event
   *
   * @see \Drupal\xero_sync\XeroSyncPropertiesEvent
   */
  const SYNC_TO_ITEM_FROM_ENTITY = 'xero_sync.sync_to_item_from_entity';

  /**
   * The name of the event fired when a Xero item is matched with an entity.
   *
   * This always has the entity type appended to it, e.g.
   * "xero_sync.sync_to_entity_from_item.user".
   *
   * This event allows you to modify the entity based on the item.
   * The entity is automatically saved after the event, so event subscribers
   * should not save the entity themselves but instead use the ::setModified()
   * method to mark the entity as needing saving.
   *
   * The event listener method receives a
   * \Drupal\xero_sync\XeroSyncPropertiesEvent instance.
   *
   * @Event
   *
   * @see \Drupal\xero_sync\XeroSyncPropertiesEvent
   */
  const SYNC_TO_ENTITY_FROM_ITEM = 'xero_sync.sync_to_entity_from_item';

  /**
   * The name of the event fired when a Xero item is being desynchronized.
   *
   * This always has the entity type of the synced Drupal entity appended to it,
   * e.g. "xero_sync.unsync_item.user".
   *
   * This event allows you to specify modifications that need to be made to the
   * item to reflect that it is no longer synchronized with the entity.
   * The event listener method receives a
   * \Drupal\xero_sync\XeroSyncPropertiesEvent instance.
   *
   * When modifying the item, the listener should use the event's
   * ::setModified() method, so that the fact that the modification has
   * happened is flagged and can be responded to properly.
   *
   * @Event
   *
   * @see \Drupal\xero_sync\XeroSyncPropertiesEvent
   */
  const UNSYNC_ITEM = 'xero_sync.unsync_item';

  /**
   * The name of the event fired when an entity is being considered for syncing.
   *
   * This always has the entity type of the entity appended to it,
   * e.g. "xero_sync.sync_is_appropriate.user".
   *
   * This event allows subscribers to specify whether an entity should be
   * synced based on properties of the entity.
   * The event listener method receives a
   * \Drupal\xero_sync\XeroSyncPropertiesEvent instance.
   *
   * When modifying the item, the listener should use the event's
   * ::setModified() method, so that the fact that the modification has
   * happened is flagged and can be responded to properly.
   *
   * @Event
   *
   * @see \Drupal\xero_sync\XeroSyncPropertiesEvent
   */
  const SYNC_IS_APPROPRIATE = 'xero_sync.sync_is_appropriate';

}
