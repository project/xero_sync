<?php

namespace Drupal\xero_sync\Event;

use Drupal\Core\Entity\EntityInterface;
use Drupal\xero\TypedData\XeroComplexItemInterface;
use Drupal\Component\EventDispatcher\Event;

/**
 * Provides a event that allows listeners to modify an entity or Xero item.
 */
class XeroSyncPropertiesEvent extends Event {

  /**
   * The Xero item.
   *
   * @var \Drupal\xero\TypedData\XeroComplexItemInterface
   */
  protected $item;

  /**
   * The entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * Whether or not event subscribers have modified something.
   *
   * @var bool
   */
  protected $modified = FALSE;

  /**
   * Constructs a Xero Sync item event object.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity being synced from/to.
   * @param \Drupal\xero\TypedData\XeroComplexItemInterface &$item
   *   The Xero item being synced from/to.
   */
  public function __construct(EntityInterface &$entity, XeroComplexItemInterface &$item = NULL) {
    $this->entity = $entity;
    $this->item = &$item;
  }

  /**
   * Gets the Xero item.
   *
   * @return \Drupal\xero\TypedData\XeroComplexItemInterface
   *   The item.
   */
  public function getItem() {
    return $this->item;
  }

  /**
   * Gets the entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The item.
   */
  public function getEntity() {
    return $this->entity;
  }

  /**
   * Gets the Xero item.
   *
   * @return \Drupal\xero\TypedData\XeroComplexItemInterface
   *   The item.
   */
  public function getItemType() {
    if ($this->getItem() instanceof XeroComplexItemInterface) {
      return $this->getItem()->getPluginId();
    }
  }

  /**
   * Gets the entity.
   *
   * @return string
   *   The type of the entity.
   */
  public function getEntityType() {
    if ($this->getEntity() instanceof EntityInterface) {
      return $this->getEntity()->getEntityTypeId();
    }
  }

  /**
   * Sets the Xero item.
   *
   * @param \Drupal\xero\TypedData\XeroComplexItemInterface $item
   *   The item.
   */
  public function setItem(XeroComplexItemInterface $item) {
    $this->item = $item;
    $this->setModified();
  }

  /**
   * Sets a property on the Xero item.
   *
   * @param string $name
   *   The property name.
   * @param mixed $value
   *   The property value.
   */
  public function setItemProperty($name, $value) {
    // We only want to flag a modification if the value has changed;
    // but properties are null by default so explicitly setting a null
    // on an unspecified property will not seem like a modification.
    $specifiedProperties = array_keys($this->getItem()->getSpecifiedProperties());
    $isPropertyUnspecified = !in_array($name, $specifiedProperties);
    if ($isPropertyUnspecified || ($this->item->get($name)->getValue() != $value)) {
      $this->item->set($name, $value);
      $this->setModified();
    }
  }

  /**
   * Flag the entity or item as having been modified by event subscribers.
   */
  public function setModified() {
    $this->modified = TRUE;
  }

  /**
   * Whether or not the entity or item has been modified by event subscribers.
   *
   * @return bool
   *   Whether or not the entity or item has been modified.
   */
  public function isModified() {
    return $this->modified;
  }

}
