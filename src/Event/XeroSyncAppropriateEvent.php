<?php

namespace Drupal\xero_sync\Event;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Component\EventDispatcher\Event;

/**
 * Allow listeners to indicate if it is appropriate to sync or unsync an entity.
 */
class XeroSyncAppropriateEvent extends Event {

  /**
   * The entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * Whether or not item creation and update is appropriate given the entity.
   *
   * @var bool
   */
  protected $flags = [
    'create' => TRUE,
    'update' => TRUE,
  ];

  /**
   * Constructs a XeroSyncAppropriateEvent object.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity being synced from.
   */
  public function __construct(EntityInterface $entity) {
    $this->entity = $entity;
  }

  /**
   * Gets the entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The entity.
   */
  public function getEntity() {
    return $this->entity;
  }

  /**
   * Gets the entity type.
   *
   * @return string|null
   *   The machine name of the entity type.
   */
  public function getEntityType() {
    if ($this->getEntity() instanceof EntityInterface) {
      return $this->getEntity()->getEntityTypeId();
    }
  }

  /**
   * Gets the appropriateness flags.
   *
   * @return array
   *   An array of booleans.
   *
   * @see \Drupal\xero_sync\XeroSyncEntityHandlerInterface::getAppropriatenessFlags()
   */
  public function getFlags() {
    return $this->flags;
  }

  /**
   * Indicate whether it is appropriate to update an item given the entity.
   *
   * @param string $flag
   *   The flag name.
   * @param bool $value
   *   The value to set on the flag.
   */
  public function setFlag($flag, $value = TRUE) {
    $this->flags[$flag] = $value;
  }

}
