<?php

namespace Drupal\xero_sync;

use Drupal\advancedqueue\Entity\Queue;
use Drupal\advancedqueue\Job;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\advancedqueue\Entity\QueueInterface as AdvancedQueueInterface;
use Drupal\Core\Entity\EntityInterface;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Creates queue jobs for syncing entities with Xero items.
 */
class XeroSyncJobManager implements XeroSyncJobManagerInterface {

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * An array of core or advanced queues.
   *
   * @var \Drupal\Core\Queue\QueueInterface[]
   */
  protected $queues;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The serializer.
   *
   * @var \Symfony\Component\Serializer\SerializerInterface
   */
  protected $serializer;

  /**
   * The queue labels used by advanced_queue.
   *
   * @var array
   */
  protected $advancedQueueLabels = [
    'xero_sync_sync' => 'Xero Sync Sync',
    'xero_sync_unsync' => 'Xero Sync Unsync',
  ];

  /**
   * Constructs a new XeroSyncEntityHandler object.
   *
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue factory.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Symfony\Component\Serializer\SerializerInterface $serializer
   *   The serializer.
   */
  public function __construct(QueueFactory $queue_factory, ModuleHandlerInterface $module_handler, SerializerInterface $serializer) {
    $this->queueFactory = $queue_factory;
    $this->moduleHandler = $module_handler;
    $this->serializer = $serializer;
  }

  /**
   * {@inheritDoc}
   */
  public function queueEntitySync(EntityInterface $entity, $create = TRUE, $update = TRUE) {
    $data = [
      'entity_id' => $entity->id(),
      'entity_type' => $entity->getEntityTypeId(),
      'entity_bundle' => $entity->bundle(),
      'create' => $create,
      'update' => $update,
    ];
    $this->enqueue('xero_sync_sync', $data);
  }

  /**
   * {@inheritDoc}
   */
  public function queueEntityUnsync(EntityInterface $entity, $itemType = NULL, $itemId = NULL, $serialize_entity = FALSE) {
    $data = [
      'entity_id' => $entity->id(),
      'entity_type' => $entity->getEntityTypeId(),
      'entity_bundle' => $entity->bundle(),
      'item_type' => $itemType,
      'item_guid' => $itemId,
    ];
    if ($serialize_entity) {
      $data['entity'] = $this->serializer->serialize($entity, 'json');
    }
    $this->enqueue('xero_sync_unsync', $data);
  }

  /**
   * Create a queue job in a specified queue.
   *
   * @param string $queueId
   *   The id of the queue to use.
   * @param array $jobData
   *   An array of data for the queue job.
   */
  protected function enqueue($queueId, array $jobData) {
    $queue = $this->getQueue($queueId);
    if ($queue instanceof AdvancedQueueInterface) {
      $job = Job::create($queueId, $jobData);
      $queue->enqueueJob($job);
    }
    else {
      $this->getQueue($queueId)->createItem($jobData);
    }
  }

  /**
   * Get a queue.
   *
   * @param string $id
   *   The id of the queue.
   *
   * @return \Drupal\Core\Queue\QueueInterface|\Drupal\advancedqueue\Entity\QueueInterface
   *   The specified queue.
   */
  protected function getQueue($id) {
    if (!isset($this->queues[$id])) {
      if ($queue = $this->getAdvancedQueue($id)) {
        $this->queues[$id] = $queue;
      }
      else {
        $this->queues[$id] = $this->queueFactory->get($id);
      }
    }
    return $this->queues[$id];
  }

  /**
   * Get an advanced queue, or create if if it doesn't exist.
   *
   * @param string $id
   *   The id of the advanced queue.
   *
   * @return \Drupal\advancedqueue\Entity\QueueInterface|null
   *   The specified advanced queue, or NULL if Advanced Queue is not installed.
   */
  protected function getAdvancedQueue($id) {
    if ($this->moduleHandler->moduleExists('advancedqueue')) {
      if (($queue = Queue::load($id)) == NULL) {
        // Add new queue.
        $data = [
          'id' => $id,
          'label' => $this->advancedQueueLabels[$id],
          'backend' => 'database',
          'backend_configuration' => ['lease_time' => 60],
          'processor' => 'cron',
          'processing_time' => 60,
          'locked' => TRUE,
        ];
        $queue = Queue::create($data);
        $queue->save();
      }
      return $queue;
    }
    return NULL;
  }

}
