<?php

namespace Drupal\xero_sync\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines the Xero Sync item finder plugin annotation object.
 *
 * Plugin namespace: Plugin\XeroSync\ItemFinder.
 *
 * @see plugin_api
 *
 * @Annotation
 */
class XeroSyncItemFinder extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The priority of the plugin.
   *
   * The higher the number, the earlier in the finding chain the plugin will be
   * executed.
   *
   * The plugin that uses the xero_sync field on an entity has priority
   * 100. Therefore plugins that rely on slower methods of identification
   * like querying Xero should generally have weights between 1 and 100.
   *
   * As a convention, plugins that create items as a fallback if no existing
   * item can be found should usually have a priority of zero or less.
   *
   * @var int
   */
  public $priority;

  /**
   * Whether there are any circumstance in which the plugin creates Xero items.
   *
   * Plugins that create new items are important, but there are circumstances
   * where that behavior isn't wanted and should be skipped.
   *
   * @var bool
   */
  public $create;

  /**
   * The entity types the plugin can find items for.
   *
   * Usually this will be just a single entity type. A empty array indicates
   * the plugin can operate on any entity type.
   *
   * @var array
   */
  public $entity_types;

}
