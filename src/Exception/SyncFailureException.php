<?php

namespace Drupal\xero_sync\Exception;

/**
 * Defines an exception thrown when syncing a Drupal entity with Xero fails.
 */
class SyncFailureException extends \Exception {

}
