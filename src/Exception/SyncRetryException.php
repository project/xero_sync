<?php

namespace Drupal\xero_sync\Exception;

/**
 * An exception thrown when syncing a Drupal entity with Xero fails temporarily.
 */
class SyncRetryException extends \Exception {

  /**
   * The retry delay.
   *
   * @var int
   */
  protected $delay = 0;

  /**
   * SyncRetryException constructor.
   *
   * @param string $message
   *   The exception message.
   * @param int $delay
   *   How many seconds to delay for.
   */
  public function __construct($message, $delay = 0) {
    parent::__construct();
    $this->delay = $delay;
  }

  /**
   * Get the retry delay.
   *
   * @return int
   *   The desired delay in seconds.
   */
  public function getDelay() {
    return $this->delay;
  }

}
