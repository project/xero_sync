<?php

namespace Drupal\xero_sync;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * An interface for an itemFinder plugin manager service.
 *
 * It manages the discovery, instantiation and chained execution of itemFinder
 * plugins.
 *
 * @see plugin_api
 */
interface XeroSyncItemFinderInterface extends PluginManagerInterface {

  /**
   * Gets a Xero item corresponding to a Drupal entity.
   *
   * Item finder plugins that service the entity type are executed successively
   * according to priority, until one of them finds an item.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to find an item for.
   * @param bool $create
   *   Whether to use plugins that may create new items if they cannot find
   *   existing items.
   *
   * @return \Drupal\xero\TypedData\XeroComplexItemInterface|null
   *   A typed data representation of the Xero item, or NULL if not found.
   *   This may be a stub, e.g. constructed from a xero_sync field with
   *   just a guid, instead of loaded complete with all properties from Xero.
   */
  public function getItem(EntityInterface $entity, $create = FALSE);

  /**
   * Clear the static cache of found items.
   */
  public function clearCache();

}
