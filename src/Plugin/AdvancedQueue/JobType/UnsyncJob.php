<?php

namespace Drupal\xero_sync\Plugin\AdvancedQueue\JobType;

/**
 * Works the Xero Sync unsync queue.
 *
 * @AdvancedQueueJobType(
 *   id = "xero_sync_unsync",
 *   label = @Translation("Xero Sync unsync"),
 *   max_retries = 7,
 *   retry_delay = 86400,
 *   ensure_unique = "overwrite"
 * )
 */
class UnsyncJob extends JobBase {}
