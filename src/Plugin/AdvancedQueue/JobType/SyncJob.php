<?php

namespace Drupal\xero_sync\Plugin\AdvancedQueue\JobType;

/**
 * Works the Xero Sync sync queue.
 *
 * @AdvancedQueueJobType(
 *   id = "xero_sync_sync",
 *   label = @Translation("Xero Sync sync"),
 *   max_retries = 7,
 *   retry_delay = 86400,
 *   ensure_unique = "overwrite"
 * )
 */
class SyncJob extends JobBase {}
