<?php

namespace Drupal\xero_sync\Plugin\XeroSync\ItemFinder;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines the interface for Xero Sync item finder plugins.
 *
 * These plugins identify or create a single Xero item that corresponds to a
 * Drupal entity, so that the item and the entity can be synchronized.
 */
interface ItemFinderInterface extends PluginInspectionInterface {

  /**
   * Gets the Xero item corresponding to an entity.
   *
   * @return \Drupal\xero\TypedData\XeroComplexItemInterface|null
   *   A typed data representation of the Xero item, or NULL if not found.
   *   This may be a stub, e.g. constructed from a xero_sync field with
   *   just a guid, instead of loaded complete with all properties from Xero.
   *   The item's "synced" property should be set to TRUE if the item is known
   *   to be already synced with the entity.
   */
  public function getItem();

  /**
   * Gets the entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The entity.
   */
  public function getEntity();

  /**
   * Sets the entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   */
  public function setEntity(EntityInterface $entity);

}
