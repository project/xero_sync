<?php

namespace Drupal\xero_sync\Plugin\XeroSync\ItemFinder;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\xero_sync\Exception\SyncFailureException;
use Drupal\xero_sync\XeroSyncUtilitiesTrait;

/**
 * Provides an item finder plugin that uses Xero Sync's default field.
 *
 * The item is "found" based on the id stored in the Xero Sync field,
 * if the entity has such a field.
 *
 * @XeroSyncItemFinder(
 *   id = "xero_sync_field",
 *   priority = 100,
 *   create = FALSE,
 *   entity_types = { },
 * )
 */
class Field extends ItemFinderBase {

  use XeroSyncUtilitiesTrait;

  /**
   * {@inheritdoc}
   */
  public function getItem() {
    $itemType = $this->getSyncedItemType($this->getEntity());
    $itemId = $this->getSyncedItemGuid($this->getEntity());
    if ($itemType && $itemId) {
      $item = $this->buildXeroItemWithId($itemType, $itemId);
      $item->synced = TRUE;
      return $item;
    }
    elseif ($itemType || $itemId) {
      $message = (string) new FormattableMarkup("Xero field values should have both a type and guid.\nType:@type Guid:@guid", [
        '@type' => $itemType,
        'guid' => $itemId,
      ]);
      throw new SyncFailureException($message);
    }
  }

}
