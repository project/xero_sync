<?php

namespace Drupal\xero_sync\Plugin\XeroSync\ItemFinder;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\TypedData\TypedDataManagerInterface;
use Drupal\xero_sync\Exception\SyncFailureException;
use Drupal\xero_sync\XeroSyncEntityHandlerInterface;
use Drupal\xero\XeroItemManagerInterface;
use Drupal\xero_sync\XeroSyncSynchronizerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\xero_sync\XeroSyncUtilitiesTrait;

/**
 * Provides the base class for item finder plugins.
 */
abstract class ItemFinderBase extends PluginBase implements ItemFinderInterface, ContainerFactoryPluginInterface {

  use XeroSyncUtilitiesTrait;

  /**
   * The Xero item manager.
   *
   * @var \Drupal\xero\XeroItemManagerInterface
   */
  protected $itemManager;

  /**
   * The typed data manager.
   *
   * @var \Drupal\Core\TypedData\TypedDataManagerInterface
   */
  protected $typedDataManager;

  /**
   * The Xero Sync entity handler.
   *
   * @var \Drupal\xero_sync\XeroSyncEntityHandlerInterface
   */
  protected $entityHandler;

  /**
   * The Xero Sync synchronizer.
   *
   * @var \Drupal\xero_sync\XeroSyncSynchronizerInterface
   */
  protected $synchronizer;

  /**
   * The entity for which the plugin should find a corresponding Xero item.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * Constructs a new ItemFinderBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\xero\XeroItemManagerInterface $item_manager
   *   The Xero item manager.
   * @param \Drupal\Core\TypedData\TypedDataManagerInterface $typed_data_manager
   *   The typed data manager.
   * @param \Drupal\xero_sync\XeroSyncEntityHandlerInterface $entity_handler
   *   The Xero Sync entity handler.
   * @param \Drupal\xero_sync\XeroSyncSynchronizerInterface $synchronizer
   *   The Xero Sync synchronizer.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, XeroItemManagerInterface $item_manager, TypedDataManagerInterface $typed_data_manager, XeroSyncEntityHandlerInterface $entity_handler, XeroSyncSynchronizerInterface $synchronizer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->itemManager = $item_manager;
    $this->typedDataManager = $typed_data_manager;
    $this->entityHandler = $entity_handler;
    $this->synchronizer = $synchronizer;
    if (!isset($configuration['entity'])) {
      throw new PluginException("An 'entity' key must be present in the ItemFinder configuration array.");
    }
    $this->setEntity($configuration['entity']);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('xero.item_manager'),
      $container->get('typed_data_manager'),
      $container->get('xero_sync.entity_handler'),
      $container->get('xero_sync.synchronizer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getEntity() {
    return $this->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setEntity(EntityInterface $entity) {
    $this->entity = $entity;
  }

  /**
   * Creates a Xero item corresponding to the entity.
   *
   * Throws a SyncFailureException if item creation is unsuccessful typically
   * due to a failure communicating with the Xero API.
   *
   * @param string $itemType
   *   The xero type of the item to create.
   *
   * @return \Drupal\xero\TypedData\XeroComplexItemInterface|null
   *   The typed data item.
   *
   * @throws \Drupal\xero_sync\Exception\SyncFailureException
   */
  protected function createItem($itemType) {
    return $this->synchronizer->createSyncedItem($this->getEntity(), NULL, $itemType);
  }

  /**
   * Find a Xero item corresponding to the entity.
   *
   * Throws a RequestException or SyncFailureException if item creation is
   * unsuccessful, typically due to a failure communicating with the Xero API.
   *
   * @param string $itemType
   *   The xero type of the item to create.
   * @param array $conditions
   *   An array of criteria. Each array value must be a subarray that is a
   *   single criterion. See /Drupal/xero/src/XeroQuery::addCondition for the
   *   required subarray structure and available options.
   *
   * @return \Drupal\xero\TypedData\XeroComplexItemInterface|null
   *   The typed data item.
   *
   * @throws \Drupal\xero_sync\Exception\SyncFailureException
   */
  protected function findItem($itemType, array $conditions) {
    $item = $this->itemManager->throwErrors()->findItem($itemType, $conditions);
    if ($item === FALSE) {
      throw new SyncFailureException("Finding an item by email failed.");
    }
    return $item;
  }

}
