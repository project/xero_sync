<?php

namespace Drupal\xero_sync\Plugin\QueueWorker;

/**
 * Works the Xero Sync sync queue.
 *
 * @QueueWorker(
 *   id = "xero_sync_sync",
 *   title = @Translation("Xero Sync sync"),
 *   cron = {"time" = 60}
 * )
 */
class SyncWorker extends WorkerBase {}
