<?php

namespace Drupal\xero_sync\Plugin\QueueWorker;

/**
 * Works the Xero Sync unsync queue.
 *
 * @QueueWorker(
 *   id = "xero_sync_unsync",
 *   title = @Translation("Xero Sync unsync"),
 *   cron = {"time" = 60}
 * )
 */
class UnsyncWorker extends WorkerBase {}
