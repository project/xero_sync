<?php

namespace Drupal\xero_sync;

use Drupal\Core\Entity\EntityInterface;
use Drupal\xero\TypedData\XeroComplexItemInterface;

/**
 * An interface for a class that synchronizes Drupal entities with Xero items.
 */
interface XeroSyncSynchronizerInterface {

  /**
   * Synchronises an entity with a Xero item.
   *
   * If an item is supplied, the caller has complete responsibility for ensuring
   * that the supplied item represents the current state of the item on Xero.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to sync with a Xero item.
   * @param \Drupal\xero\TypedData\XeroComplexItemInterface|null $item
   *   (optional) The Xero item to sync the entity with.
   * @param bool $create
   *   (optional) Whether to create a Xero item if no corresponding item exists.
   * @param bool $update
   *   (optional) Whether to update an existing corresponding Xero item.
   * @param \Drupal\Core\Entity\EntityInterface|null $previousEntityState
   *   (optional) The previous state of the entity, before the changes that
   *   are now being synchronised. Often from $entity->original.
   *
   *   returns Drupal\xero\TypedData\XeroComplexItemInterface|NULL
   *   The Xero item, or null if no corresponding item can be found.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\xero_sync\Exception\SyncFailureException
   */
  public function syncEntityWithItem(
    EntityInterface $entity,
    XeroComplexItemInterface $item = NULL,
    $create = TRUE,
    $update = TRUE,
    $previousEntityState = NULL
    );

  /**
   * Desynchronizes a Xero item with an entity.
   *
   * Sometimes there is information on Xero items that should not
   * be kept if the entity is later thought to correspond to a different item.
   * This method only modifies the item, not the entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to desynchronize.
   * @param \Drupal\xero\TypedData\XeroComplexItemInterface $item
   *   (optional) The Xero item to be updated.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\xero_sync\Exception\SyncFailureException
   */
  public function unsyncEntityWithItem(EntityInterface $entity, XeroComplexItemInterface $item = NULL);

  /**
   * Creates a Xero item from an entity.
   *
   * A SyncFailureException will be thrown if the Xero Item manager cannot
   * create the item, likely due to a failure calling the Xero API.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to update the item from.
   * @param \Drupal\xero\TypedData\XeroComplexItemInterface $item
   *   (optional) The Xero item to be created, probably a stub.
   * @param string $xeroType
   *   (optional) The type Xero item to create. Required if item not provided.
   *
   * @return \Drupal\xero\TypedData\XeroComplexItemInterface
   *   The latest version of the item from Xero.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\xero_sync\Exception\SyncFailureException
   */
  public function createSyncedItem(EntityInterface $entity, XeroComplexItemInterface $item = NULL, $xeroType = NULL);

  /**
   * Updates a Xero item based on an entity.
   *
   * The supplied item is used as the basis for determining if the item needs
   * to be updated on Xero. The caller has complete responsibility for ensuring
   * that the supplied item represents the current state of the item on Xero,
   * unless a previous state of the entity is supplied.
   *
   * If the supplied item is not being unsynced, and is marked as synced but is
   * a stub and we do know the previous state of the entity, then the item
   * suggested by the previous state of the entity is used as the basis of
   * comparison for determining if an update is needed now.
   *
   * A SyncFailureException will be thrown if the Xero Item manager cannot
   * update the item, likely due to a failure calling the Xero API.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to update the item from.
   * @param \Drupal\xero\TypedData\XeroComplexItemInterface $item
   *   The Xero item to be updated, possibly a stub.
   * @param bool $unsync
   *   (optional) Whether the item is being unsynced.
   * @param \Drupal\Core\Entity\EntityInterface|null $previousEntityState
   *   (optional) The previous state of the entity, before the changes that
   *   are now being synchronised. Often from $entity->original.
   *
   * @return \Drupal\xero\TypedData\XeroComplexItemInterface
   *   The latest version of the item from Xero if update was needed, otherwise
   *   the supplied item.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\xero_sync\Exception\SyncFailureException
   */
  public function updateSyncedItemIfNecessary(EntityInterface $entity, XeroComplexItemInterface $item, $unsync = FALSE, $previousEntityState = NULL);

  /**
   * Determines whether an item needs updating based on an entity.
   *
   * This can be useful in determining whether to queue an item for later
   * updating, because information about the previous state of an updated
   * entity may not available later so the decision itself cannot be deferred.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to update the item from.
   * @param \Drupal\xero\TypedData\XeroComplexItemInterface $item
   *   The Xero item to be updated, possibly a stub.
   * @param \Drupal\Core\Entity\EntityInterface|null $previousEntityState
   *   (optional) The previous state of the entity, before the changes that
   *   are now being synchronised. Often from $entity->original.
   *
   * @return bool
   *   Whether the item needs updating on Xero.
   *
   * @see ::updateSyncedItemIfNecessary
   */
  public function isItemUpdateNeeded(EntityInterface $entity, XeroComplexItemInterface $item, $previousEntityState = NULL);

  /**
   * Update an entity from a Xero item.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to update.
   * @param \Drupal\xero\TypedData\XeroComplexItemInterface|null $item
   *   The Xero item to update from.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The updated entity.
   */
  public function syncToEntityFromItem(EntityInterface $entity, XeroComplexItemInterface $item);

}
