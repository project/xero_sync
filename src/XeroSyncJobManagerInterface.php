<?php

namespace Drupal\xero_sync;

use Drupal\Core\Entity\EntityInterface;

/**
 * An interface for creating queue jobs for syncing entities with Xero items.
 */
interface XeroSyncJobManagerInterface {

  /**
   * Place a job in a queue to sync a Drupal entity with a Xero item.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   An entity.
   * @param bool $create
   *   (optional) Whether to create a Xero item if no matching one exists.
   * @param bool $update
   *   (optional) Whether to update the matched Xero item if it exists.
   */
  public function queueEntitySync(EntityInterface $entity, $create = TRUE, $update = TRUE);

  /**
   * Place a job in a queue to unsync a Drupal entity with a Xero item.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   An entity.
   * @param string $itemType
   *   (optional) The data type of the Xero item.
   * @param string $itemId
   *   (optional) The id of the Xero item.
   * @param bool $serialize_entity
   *   (optional) Whether to serialize the entity, e.g. if entity is deleted.
   */
  public function queueEntityUnsync(EntityInterface $entity, $itemType = NULL, $itemId = NULL, $serialize_entity = FALSE);

}
