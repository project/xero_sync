<?php

/**
 * @file
 * Xero Sync User Contact module.
 *
 * Synchronizes users in Drupal with contacts on Xero.
 */

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\UserInterface;

/**
 * Implements hook_entity_base_field_info().
 */
function xero_sync_user_contact_entity_base_field_info(EntityTypeInterface $entity_type) {
  $fields = [];
  if ($entity_type->id() === 'user') {
    $fieldId = \Drupal::service('xero_sync.entity_handler')->getFieldName('user');
    $fields[$fieldId] = BaseFieldDefinition::create('xero_reference')
      ->setLabel(t('Xero Contact ID'))
      ->setDescription(t('The ContactID of the Xero Contact synchronized with this user.'));
  }
  return $fields;
}

/**
 * Implements hook_ENTITY_TYPE_insert().
 */
function xero_sync_user_contact_user_insert(UserInterface $user) {
  \Drupal::service('xero_sync.entity_handler')
    ->handleInsert($user);
}

/**
 * Implements hook_ENTITY_TYPE_update().
 */
function xero_sync_user_contact_user_update(UserInterface $user) {
  \Drupal::service('xero_sync.entity_handler')
    ->handleUpdate($user);
}

/**
 * Implements hook_ENTITY_TYPE_delete().
 */
function xero_sync_user_contact_user_delete(UserInterface $user) {
  \Drupal::service('xero_sync.entity_handler')
    ->handleDelete($user);
}
