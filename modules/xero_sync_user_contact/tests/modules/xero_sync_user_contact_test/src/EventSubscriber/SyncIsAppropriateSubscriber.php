<?php

namespace Drupal\xero_sync_user_contact_test\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\xero_sync\Event\XeroSyncEvents;
use Drupal\xero_sync\Event\XeroSyncAppropriateEvent;
use Drupal\user\UserInterface;

/**
 * Restrict syncing to admin users only.
 */
class SyncIsAppropriateSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      XeroSyncEvents::SYNC_IS_APPROPRIATE . '.user' => 'syncAdminsOnly',
    ];
    return $events;
  }

  /**
   * Restrict syncing to admin users only.
   *
   * @param \Drupal\xero_sync\Event\XeroSyncAppropriateEvent $event
   *   The Xero Sync appropriate event.
   */
  public function syncAdminsOnly(XeroSyncAppropriateEvent $event) {
    if ($event->getEntityType() === 'user' && $event->getEntity() instanceof UserInterface) {
      $user = $event->getEntity();
      if (!$user->hasRole('admin')) {
        $event->setFlag('create', FALSE);
        $event->setFlag('update', FALSE);
      }
    }
  }

}
