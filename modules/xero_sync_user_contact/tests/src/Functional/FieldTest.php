<?php

namespace Drupal\Tests\xero_sync_user_contact\Functional;

use Drupal\node\Entity\Node;
use Drupal\Tests\BrowserTestBase;
use Drupal\user\Entity\User;

/**
 * Tests the module provides the expected field in users.
 *
 * @group xero_sync
 */
class FieldTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'user',
    'xero_sync_user_contact',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Test the field exists.
   */
  public function testFieldExist() {
    $node = Node::create(['type' => 'article']);
    $fields = $node->getFields(FALSE);
    $this->assertArrayNotHasKey('xero_sync', $fields);

    $user = User::create([]);
    $fields = $user->getFields(FALSE);
    $this->assertArrayHasKey('xero_sync', $fields);
  }

}
