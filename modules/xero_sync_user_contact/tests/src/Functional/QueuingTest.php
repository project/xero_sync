<?php

namespace Drupal\Tests\xero_sync_user_contact\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\user\Entity\User;
use Radcliffe\Xero\XeroClient;

/**
 * Tests the module queues a job when an entity is created.
 *
 * @group xero_sync
 */
class QueuingTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'user',
    'xero_sync',
    'xero_sync_user_contact',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $client = $this->createMock(XeroClient::class);
    $this->container->set('xero.client', $client);

    // Remove queue items hanging around from module install.
    $queue = \Drupal::queue('xero_sync_sync');
    while ($queue->numberOfItems() > 0) {
      $queue->deleteItem($queue->claimItem());
    }
  }

  /**
   * Test queuing when user is updated.
   */
  public function testQueuingWhenUserIsCreated() {
    $user = User::create([
      'uid' => 23,
      'name' => 'Jordan',
    ]);
    $user->enforceIsNew(TRUE);
    $user->save();

    $user = User::create([
      'uid' => 33,
      'name' => 'Bird',
      'xero_sync' =>
        [
          'type' => 'xero_contact',
          'guid' => 'some guid',
        ],
    ]);
    $user->enforceIsNew(TRUE);
    $user->save();

    // Both are queued, even though one already had a synced xero item.
    $queue = \Drupal::queue('xero_sync_sync');
    $this->assertEquals(2, $queue->numberOfItems());
    $item = $queue->claimItem();
    $this->assertEquals($item->data['entity_id'], 23);
    $this->assertEquals($item->data['entity_type'], 'user');
    $item = $queue->claimItem();
    $this->assertEquals($item->data['entity_id'], 33);
    $this->assertEquals($item->data['entity_type'], 'user');
  }

  /**
   * Test queuing when user is changed.
   */
  public function testQueuingWhenUserIsUpdatedIfChanged() {
    // Create the user.
    $user = User::create([
      'uid' => 23,
      'name' => 'Bird',
      'xero_sync' => [
        'type' => 'xero_contact',
        'guid' => 'some guid',
      ],
    ]);
    $user->save();
    $queue = \Drupal::queue('xero_sync_sync');
    $queue->deleteItem($queue->claimItem());
    $this->assertEquals(0, $queue->numberOfItems());

    // Now change the user in a way that will trigger an update.
    $user->set('name', 'new name');
    $user->save();
    $this->assertEquals(1, $queue->numberOfItems());
    $item = $queue->claimItem();
    $this->assertEquals($item->data['entity_id'], 23);
    $this->assertEquals($item->data['entity_type'], 'user');
  }

  /**
   * Test queuing when user is updated in ways that don't need syncing.
   */
  public function testQueuingWhenUserIsUpdatedIfChangedIrrelevantly() {
    // Create the user.
    $user = User::create([
      'uid' => 23,
      'name' => 'Bird',
      'xero_sync' =>
        [
          'type' => 'xero_contact',
          'guid' => 'some guid',
        ],
    ]);
    $user->save();
    $queue = \Drupal::queue('xero_sync_sync');
    $queue->deleteItem($queue->claimItem());
    $this->assertEquals(0, $queue->numberOfItems());

    // Changing the timezone should not cause an update.
    $user->set('timezone', 'Europe/Paris');
    $user->save();
    $this->assertEquals(0, $queue->numberOfItems());
  }

}
