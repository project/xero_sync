<?php

namespace Drupal\Tests\xero_sync_user_contact\Functional;

use Drupal\Core\Extension\MissingDependencyException;
use Drupal\Tests\BrowserTestBase;
use Drupal\user\Entity\User;

/**
 * Tests the module provides the expected field in users.
 *
 * @group xero_sync
 */
class InstallTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Test module installation.
   */
  public function testInstall() {
    try {
      $success = $this->container->get('module_installer')->install(['xero_sync_user_contact'], TRUE);
      $this->assertTrue($success, 'Enabled xero_sync_user_contact');
    }
    catch (MissingDependencyException $e) {
      // The exception message has all the details.
      $this->fail($e->getMessage());
    }
    $this->rebuildContainer();

    // There are two users by default if node is installed.
    $users = User::loadMultiple();
    $this->assertEquals(2, count($users));

    // Existing users should be queued for syncing.
    // The order of the users in the queue seems random.
    $queue = \Drupal::queue('xero_sync_sync');
    $this->assertEquals(2, $queue->numberOfItems());
    $item = $queue->claimItem();
    $id1 = $item->data['entity_id'];
    $item = $queue->claimItem();
    $id2 = $item->data['entity_id'];
    $ids = [$id1, $id2];
    sort($ids);
    $this->assertEquals([0, 1], $ids);

  }

}
