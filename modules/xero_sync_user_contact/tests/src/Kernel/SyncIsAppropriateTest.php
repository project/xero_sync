<?php

namespace Drupal\Tests\xero_sync_user_contact\Kernel;

/**
 * Test using an event subscriber to restrict syncing to admin users only.
 *
 * The xero_sync_user_contact_test module contains an event subscriber that
 * provides this for this test.
 *
 * @group xero_sync
 */
class SyncIsAppropriateTest extends XeroSyncUserContactTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'user',
    'serialization',
    'xero',
    'xero_sync',
    'xero_sync_user_contact',
    'xero_sync_user_contact_test',
  ];

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->mockItemManager();
    $this->bypassQueues();
  }

  /**
   * Test what happens when a non-admin user is created.
   */
  public function testInsertNonAdmin() {
    $itemManagerExpectations = [];
    $this->assertSuccessfulInsert($itemManagerExpectations);

    $itemManagerExpectations = [
      $this->getFindByEmailExpectation(FALSE),
      $this->getCreateExpectation(),
    ];
    $this->user->addRole('admin');
    $this->assertUserSave(TRUE, $itemManagerExpectations);
  }

  /**
   * Test what happens when an admin user is created.
   */
  public function testInsertAdmin() {
    $this->user->addRole('admin');
    $itemManagerExpectations = [
      $this->getFindByEmailExpectation(FALSE),
      $this->getCreateExpectation(),
    ];
    $this->assertSuccessfulInsert($itemManagerExpectations);
  }

}
