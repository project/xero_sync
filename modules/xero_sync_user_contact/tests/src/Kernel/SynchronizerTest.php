<?php

namespace Drupal\Tests\xero_sync_user_contact\Kernel;

use Drupal\xero_sync\Exception\SyncFailureException;

/**
 * Test the synchronizer service.
 *
 * @group xero_sync
 */
class SynchronizerTest extends XeroSyncUserContactTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'user',
    'serialization',
    'xero',
    'xero_sync',
    'xero_sync_user_contact',
  ];

  /**
   * The Xero Sync synchronizer.
   *
   * @var \Drupal\xero_sync\XeroSyncSynchronizer
   */
  protected $synchronizer;

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->mockItemManager();
    $this->disableEntityHandler();
    $this->user->save();
    $this->synchronizer = \Drupal::service('xero_sync.synchronizer');
  }

  /**
   * Test syncEntityWithItem with an email match.
   */
  public function testNotSyncedSyncEntityWithItemByEmail() {
    $this->expectEntityUpdates(1);
    $this->itemManagerExpects([
      $this->getFindByEmailExpectation(TRUE),
      $this->getUpdateExpectation(),
    ]);
    $this->synchronizer->syncEntityWithItem($this->user);
    $this->assertItemManagerExpectationsMet();
    $this->assertItemReferencedInField($this->user, 'xero_contact', $this->xeroId);
  }

  /**
   * Test syncEntityWithItem with an email match and updating disallowed.
   */
  public function testNotSyncedSyncEntityWithItemByEmailNoUpdate() {
    $this->expectEntityUpdates(1);
    $this->itemManagerExpects([
      $this->getFindByEmailExpectation(TRUE),
    ]);
    $this->synchronizer->syncEntityWithItem($this->user, NULL, TRUE, FALSE);
    $this->assertItemManagerExpectationsMet();
    $this->assertItemReferencedInField($this->user, 'xero_contact', $this->xeroId);
  }

  /**
   * Test syncEntityWithItem when creation becomes required.
   */
  public function testNotSyncedSyncEntityWithItemCreate() {
    $this->expectEntityUpdates(1);
    $this->itemManagerExpects([
      $this->getFindByEmailExpectation(FALSE),
      $this->getCreateExpectation(),
    ]);
    $this->synchronizer->syncEntityWithItem($this->user);
    $this->assertItemManagerExpectationsMet();
    $this->assertItemReferencedInField($this->user, 'xero_contact', $this->xeroId);
  }

  /**
   * Test syncEntityWithItem when creation becomes required.
   */
  public function testItemCreationFailed() {
    $this->expectEntityUpdates(0);
    $this->itemManagerExpects([
      $this->getFindByEmailExpectation(FALSE),
      $this->getFailedCreateExpectation(),
    ]);
    $this->expectException(SyncFailureException::class);
    $this->synchronizer->syncEntityWithItem($this->user);
    $this->assertItemManagerExpectationsMet();
  }

  /**
   * Test syncEntityWithItem with an already stored contact and no update.
   */
  public function testSyncedSyncEntityWithItemSyncNoUpdate() {
    $this->expectEntityUpdates(1);
    $this->setXeroField($this->user, 'xero_contact', $this->xeroId);
    $this->user->save();
    $this->ItemManagerExpects([]);
    $this->synchronizer->syncEntityWithItem($this->user, NULL, TRUE, FALSE);
    $this->assertItemManagerExpectationsMet();
    $this->assertItemReferencedInField($this->user, 'xero_contact', $this->xeroId);
  }

  /**
   * Test syncEntityWithItem with an already stored contact and update allowed.
   */
  public function testSyncedSyncEntityWithItemWithUpdate() {
    $this->expectEntityUpdates(1);
    $this->setXeroField($this->user, 'xero_contact', $this->xeroId);
    $this->user->save();

    $this->itemManagerExpects([
      $this->getUpdateExpectation(),
    ]);
    $this->synchronizer->syncEntityWithItem($this->user);
    $this->assertItemManagerExpectationsMet();
    $this->assertItemReferencedInField($this->user, 'xero_contact', $this->xeroId);
  }

  /**
   * Test createSyncedItem with an item supplied.
   */
  public function testcreateSyncedItemWithItem() {
    $this->expectEntityUpdates(0);
    $this->itemManagerExpects([
      $this->getCreateExpectation(),
    ]);
    $this->synchronizer->createSyncedItem($this->user, $this->getCreatingContact());
    $this->assertItemManagerExpectationsMet();
  }

  /**
   * Test createSyncedItem with only a xero type supplied.
   */
  public function testCreateSyncedItemWithXeroType() {
    $this->expectEntityUpdates(0);
    $this->itemManagerExpects([
      $this->getCreateExpectation(),
    ]);
    $this->synchronizer->createSyncedItem($this->user, NULL, 'xero_contact');
    $this->assertItemManagerExpectationsMet();
  }

  /**
   * Test updateSyncedItemIfNecessary with an item that needs updating.
   */
  public function testUpdateSyncedItemUpdateNeeded() {
    $this->expectEntityUpdates(0);
    $item = $this->buildXeroItem('xero_contact', [
      'ContactID' => $this->xeroId,
      'ContactNumber' => $this->uid,
      'Name' => 'wrong name',
      'EmailAddress' => $this->email,
    ]);
    $this->itemManagerExpects([
      $this->getUpdateExpectation(),
    ]);
    $this->synchronizer->updateSyncedItemIfNecessary($this->user, $item);
    $this->assertItemManagerExpectationsMet();
  }

  /**
   * Test updateSyncedItemIfNecessary with an item that does not need updating.
   */
  public function testUpdateSyncedItemNoUpdateNeeded() {
    $this->expectEntityUpdates(0);
    $this->itemManagerExpects([]);
    $this->synchronizer->updateSyncedItemIfNecessary($this->user, $this->getUpdatedContact());
    $this->assertItemManagerExpectationsMet();
  }

  /**
   * Test updateSyncedItemIfNecessary with an item that needs updating.
   *
   * It needs updating despite the known previous state.
   */
  public function testUpdateSyncedItemUpdateNeededDespitePreviousState() {
    $this->expectEntityUpdates(0);
    $contact = $this->getStubContact();
    $contact->stub = TRUE;
    $contact->synced = TRUE;

    // The previous state has a different name to the current state so an
    // update is needed.
    $previousUserState = clone $this->user;
    $previousUserState->set('name', 'wrong name');

    $this->itemManagerExpects([
      $this->getUpdateExpectation(),
    ]);
    $this->synchronizer->updateSyncedItemIfNecessary($this->user, $contact, FALSE, $previousUserState);
    $this->assertItemManagerExpectationsMet();
  }

  /**
   * Test updateSyncedItemIfNecessary with an item that does not need updating.
   *
   * It does not need updating because of the known previous state.
   */
  public function testUpdateSyncedItemUpdateNotNeededDueToPreviousState() {
    $this->expectEntityUpdates(1);
    $contact = $this->getStubContact();
    $contact->stub = TRUE;
    $contact->synced = TRUE;

    // The previous state is different to the current state, but not in a way
    // that is relevant.
    $previousUserState = clone $this->user;
    $this->user->set('timezone', "Europe/Paris");
    $this->user->save();

    $this->itemManagerExpects([]);
    $this->synchronizer->updateSyncedItemIfNecessary($this->user, $contact, FALSE, $previousUserState);
    $this->assertItemManagerExpectationsMet();
  }

  /**
   * Test unsyncEntityWithItem with the stored item.
   */
  public function testUnsyncEntityWithStoredItem() {
    $this->expectEntityUpdates(1);
    $this->setXeroField($this->user, 'xero_contact', $this->xeroId);
    $this->user->save();
    $this->itemManagerExpects([
      $this->getUnsyncExpectation(),
    ]);
    $this->synchronizer->unsyncEntityWithItem($this->user);
    $this->assertItemManagerExpectationsMet();
  }

  /**
   * Test unsyncEntityWithItem with a specified item.
   */
  public function testUnsyncEntityWithSpecifiedItem() {
    $this->expectEntityUpdates(0);
    $unsyncingContact = clone $this->getUpdatedContact();
    $unsyncingContact->get('ContactNumber')->setValue('');
    $expectation = [
      'method' => 'updateItem',
      'arguments' => [
        $unsyncingContact,
      ],
      'return' => $this->getFoundContact(),
    ];
    $this->itemManagerExpects([
      $expectation,
    ]);
    $this->synchronizer->unsyncEntityWithItem($this->user, $this->getUpdatedContact());
    $this->assertItemManagerExpectationsMet();
  }

  /**
   * Test syncEntityWithItem with two calls, ensure recursion checking is unset.
   */
  public function testSyncEntityWithItemSuccessivelyRecursionChecking() {
    $this->expectEntityUpdates(1);
    $this->itemManagerExpects([
      $this->getFindByEmailExpectation(FALSE),
      $this->getFindByEmailExpectation(FALSE),
      $this->getCreateExpectation(),
    ]);
    $this->synchronizer->syncEntityWithItem($this->user, NULL, FALSE, TRUE);
    $this->synchronizer->syncEntityWithItem($this->user, NULL, TRUE, TRUE);
    $this->assertItemManagerExpectationsMet();
    $this->assertItemReferencedInField($this->user, 'xero_contact', $this->xeroId);
  }

}
