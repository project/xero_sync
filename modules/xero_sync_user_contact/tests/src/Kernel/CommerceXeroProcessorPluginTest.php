<?php

namespace Drupal\Tests\xero_sync_user_contact\Kernel;

use Drupal\xero_sync_user_contact\Plugin\CommerceXero\processor\CustomerContact;
use Drupal\commerce_price\Price;
use Drupal\commerce_xero\Plugin\CommerceXero\type\Invoice;
use Prophecy\Prophecy\ObjectProphecy;
use Psr\Log\LoggerInterface;
use Prophecy\Prophet;

/**
 * Tests the Xero Sync User Contact processor plugin for Commerce Xero.
 *
 * This **must** be a kernel test because Invoice and Payment contain circular
 * references. It's based on:
 * Drupal\Tests\commerce_xero\Kernel\Plugin\CommerceXero\type\InvoiceTest.
 *
 * @requires module entity_reference_revisions
 * @requires module profile
 * @requires module state_machine
 * @requires module commerce
 * @requires module commerce_xero
 * @requires module xero
 * @group xero_sync
 */
class CommerceXeroProcessorPluginTest extends XeroSyncUserContactTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'entity_reference_revisions',
    'profile',
    'state_machine',
    'commerce',
    'commerce_order',
    'commerce_store',
    'commerce_price',
    'commerce_payment',
    'commerce_payment_example',
    'commerce_xero',
    'xero',
    'serialization',
    'xero_sync',
    'xero_sync_user_contact',
  ];

  /**
   * Test the CustomerContact plugin.
   *
   * It sets a contact on an invoice by syncing a contact with the order
   * customer.
   *
   * @param bool $anonymousCustomer
   *   Whether the order customer is anonymous.
   * @param bool $isContactFound
   *   Whether a matching contact is found (even created) by the item finder.
   *
   * @dataProvider invoiceContactProvider
   */
  public function testInvoiceContact(
    $anonymousCustomer,
    $isContactFound
    ) {
    $prophet = new Prophet();

    $email = $this->randomMachineName() . "@example.com";
    $name = $anonymousCustomer ? 'Anonymous' : $this->randomMachineName();
    $contactID = $this->randomMachineName();

    $customerProphet = $prophet->prophesize('\Drupal\user\UserInterface');
    $customerProphet->getAccountName()->willReturn($name);
    $customerProphet->isAnonymous()->willReturn($anonymousCustomer);

    $profileProphet = $prophet->prophesize('\Drupal\profile\Entity\ProfileInterface');
    $profileProphet->hasField('address')->willReturn(FALSE);

    $orderProphet = $prophet->prophesize('\Drupal\commerce_order\Entity\OrderInterface');
    $orderProphet->getBillingProfile()->willReturn($profileProphet->reveal());
    $orderProphet->getCustomer()->willReturn($customerProphet->reveal());
    $orderProphet->getEmail()->willReturn($email);

    $synchronizerProphet = $prophet->prophesize('\Drupal\xero_sync\XeroSyncSynchronizerInterface');

    $foundContact = $this->buildXeroItem('xero_contact', [
      'ContactID' => $contactID,
      'Name' => $name,
      'Email' => $email,
    ]);
    $synchronizerReturn = $isContactFound ? $foundContact : NULL;
    $synchronizerProphet->syncEntityWithItem($customerProphet->reveal(), NULL, TRUE, TRUE)->willReturn($synchronizerReturn);
    $this->container->set('xero_sync.synchronizer', $synchronizerProphet->reveal());

    $logger = $this->createMock(LoggerInterface::class);
    $this->container->set('logger.channel.xero_sync', $logger);

    $definition = [
      'id' => 'xero_sync_user_contact_customer',
      'label' => 'Xero Sync User Contact Customer',
      'types' => ['xero_invoice'],
      'settings' => [],
      'required' => FALSE,
    ];
    $plugin = CustomerContact::create(
      $this->container,
      [],
      'xero_sync_user_contact_customer',
      $definition
    );

    $invoice = $this->getInvoice($orderProphet);
    $paymentProphet = $this->getPaymentProphet($orderProphet);
    $success = $plugin->process($paymentProphet->reveal(), $invoice, $this->getStrategyProphet()->reveal());
    $this->assertTrue($success);

    $contact = $invoice->get('Contact');
    if ($anonymousCustomer || !$isContactFound) {
      // If sync was unsuccessful, the default values from
      // commerce_xero/OrderDataTransformationTrait should be used.
      $this->assertEquals($name, $contact->get('Name')->getString());
      $this->assertEquals($email, $contact->get('EmailAddress')->getString());
      $contactID = '';
    }
    $this->assertEquals($contactID, $contact->get('ContactID')->getString());
  }

  /**
   * Test scenarios for testInvoiceContact.
   *
   * @return array
   *   Scenarios.
   */
  public function invoiceContactProvider() {
    return [
      'Contact found by customer' => [
        'anonymousCustomer' => FALSE,
        'isContactFound' => TRUE,
      ],

      'Contact not found by customer' => [
        'anonymousCustomer' => FALSE,
        'isContactFound' => FALSE,
      ],

      'Anonymous customer' => [
        'anonymousCustomer' => TRUE,
        'isContactFound' => FALSE,
      ],
    ];
  }

  /**
   * Get a Xero invoice typed data object via Commerce Xero.
   *
   * @param \Prophecy\Prophecy\ObjectProphecy $orderProphet
   *   An order prophet.
   *
   * @return \Drupal\Core\TypedData\TypedDataInterface|\Drupal\xero\TypedData\XeroTypeInterface
   *   The invoice.
   */
  protected function getInvoice(ObjectProphecy $orderProphet) {
    $paymentProphet = $this->getPaymentProphet($orderProphet);
    $definition = [
      'id' => 'commerce_xero_invoice',
      'label' => 'Invoice',
      'type' => 'xero_invoice',
      'settings' => [],
    ];
    $plugin = new Invoice($definition, 'commerce_xero_invoice', $definition);
    $invoice = $plugin->make($paymentProphet->reveal(), $this->getStrategyProphet()->reveal());
    $this->assertInstanceOf('\Drupal\xero\Plugin\DataType\Invoice', $invoice);
    return $invoice;
  }

  /**
   * Get a strategy prophet.
   *
   * @return \Prophecy\Prophecy\ObjectProphecy
   *   A strategy prophet.
   */
  protected function getStrategyProphet() {
    $prophet = new Prophet();

    $strategyProphet = $prophet->prophesize('\Drupal\commerce_xero\Entity\CommerceXeroStrategyInterface');
    $strategyProphet->getBankAccountID()->willReturn('500');
    $strategyProphet->getRevenueAccountCode()->willReturn('200');
    return $strategyProphet;
  }

  /**
   * Get a payment prophet.
   *
   * @param \Prophecy\Prophecy\ObjectProphecy $orderProphet
   *   An order prophet.
   *
   * @return \Prophecy\Prophecy\ObjectProphecy
   *   A payment prophet.
   */
  protected function getPaymentProphet(ObjectProphecy $orderProphet) {
    $prophet = new Prophet();
    $unitprice1 = new Price('2.00', 'USD');
    $unitprice2 = new Price('5.99', 'USD');
    $quantity1 = 2;
    $quantity2 = 1;
    $totalprice1 = new Price($unitprice1->getNumber() * $quantity1, 'USD');
    $totalprice2 = new Price($unitprice2->getNumber() * $quantity2, 'USD');
    $totalprice = new Price('9.99', 'USD');

    $item1Prophet = $prophet->prophesize('\Drupal\commerce_order\Entity\OrderItemInterface');
    $item1Prophet->getTitle()->willReturn('Product 1');
    $item1Prophet->getQuantity()->willReturn($quantity1);
    $item1Prophet->getUnitPrice()->willReturn($unitprice1);
    $item1Prophet->getTotalPrice()->willReturn($totalprice1);
    $item1Prophet->getAdjustedTotalPrice()->willReturn($totalprice1);
    $item2Prophet = $prophet->prophesize('\\Drupal\commerce_order\Entity\OrderItemInterface');
    $item2Prophet->getTitle()->willReturn('Product 2');
    $item2Prophet->getQuantity()->willReturn($quantity2);
    $item2Prophet->getUnitPrice()->willReturn($unitprice2);
    $item2Prophet->getTotalPrice()->willReturn($totalprice2);
    $item2Prophet->getAdjustedTotalPrice()->willReturn($totalprice2);
    $orderProphet->id()->willReturn(1);
    $orderProphet->getCreatedTime()->willReturn(date('U'));
    $orderProphet->getAdjustments()->willReturn([]);
    $orderProphet->getTotalPrice()->willReturn($totalprice);
    $orderProphet->getSubtotalPrice()->willReturn($totalprice);
    $orderProphet->getOrderNumber()->willReturn('12345');
    $orderProphet
      ->getItems()
      ->willReturn([$item1Prophet->reveal(), $item2Prophet->reveal()]);

    $paymentEntityProphet = $prophet->prophesize('\Drupal\commerce_payment\Entity\PaymentInterface');
    $paymentEntityProphet->id()->willReturn(1);
    $paymentEntityProphet->getOrder()->willReturn($orderProphet->reveal());
    return $paymentEntityProphet;
  }

}
