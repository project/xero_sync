<?php

namespace Drupal\Tests\xero_sync_user_contact\Kernel;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\xero\TypedData\XeroComplexItemInterface;
use Drupal\xero_sync\XeroSyncEntityHandler;

/**
 * Test the insert method of the user handler.
 *
 * @group xero_sync
 */
class ItemFinderTest extends XeroSyncUserContactTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'user',
    'serialization',
    'xero',
    'xero_sync',
    'xero_sync_user_contact',
  ];

  /**
   * The item finder.
   *
   * @var \Drupal\xero_sync\XeroSyncItemFinder
   */
  protected $itemFinder;

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->mockItemManager();
    $this->disableEntityHandler();
    $this->expectEntityUpdates(0);
    $this->itemFinder = \Drupal::service('plugin.manager.xero_sync_item_finder');
  }

  /**
   * Test that all the plugins are discovered, and their annotations parsed.
   */
  public function testPluginsDiscovered() {
    $actual = $this->itemFinder->getDefinitions();
    $expected = [
      'xero_sync_field' => [
        'id' => 'xero_sync_field',
        'priority' => 100,
        'create' => FALSE,
        'entity_types' => [],
        'class' => 'Drupal\xero_sync\Plugin\XeroSync\ItemFinder\Field',
        'provider' => 'xero_sync',
      ],
      'xero_sync_user_contact_create' => [
        'id' => 'xero_sync_user_contact_create',
        'priority' => 0,
        'create' => TRUE,
        'entity_types' => ['user'],
        'class' => 'Drupal\xero_sync_user_contact\Plugin\XeroSync\ItemFinder\Create',
        'provider' => 'xero_sync_user_contact',
      ],
      'xero_sync_user_contact_email' => [
        'id' => 'xero_sync_user_contact_email',
        'priority' => 50,
        'create' => FALSE,
        'entity_types' => ['user'],
        'class' => 'Drupal\xero_sync_user_contact\Plugin\XeroSync\ItemFinder\Email',
        'provider' => 'xero_sync_user_contact',
      ],
    ];
    $message = (string) new FormattableMarkup("The plugin definitions discovered did not match those expected.  \n Actual: \n @actual \n \n Expected: \n @expected", [
      '@actual' => print_r($expected, TRUE),
      '@expected' => print_r($expected, TRUE),
    ]);
    $this->assertEquals($expected, $actual, $message);
  }

  /**
   * Test finding an item when there is a value stored in the field.
   */
  public function testFindByField() {
    $this->user->set(XeroSyncEntityHandler::DEFAULT_FIELD_NAME, [
      'type' => 'xero_contact',
      'guid' => $this->xeroId,
    ]);
    $this->user->save();
    $this->assertItemReferencedInField($this->user, 'xero_contact', $this->xeroId);

    $item = $this->itemFinder->getItem($this->user);

    // The query manager should not have been called.
    $this->assertItemManagerExpectationsMet();

    $this->assertInstanceOf(XeroComplexItemInterface::class, $item);
    $this->assertEquals($this->xeroId, $item->get('ContactID')->getString());
    $this->assertEquals('xero_contact', $item->getPluginId());
  }

  /**
   * Test when a contact with the same email is found.
   */
  public function testFindByEmail() {
    $this->itemManagerExpects([
      $this->getFindByEmailExpectation(TRUE),
    ]);
    $item = $this->itemFinder->getItem($this->user);
    $this->assertSame($this->foundContact, $item);
    $this->assertItemManagerExpectationsMet();
  }

  /**
   * Test when an existing contact cannot be found, and creation is allowed.
   */
  public function testCreate() {
    $this->itemManagerExpects([
      $this->getFindByEmailExpectation(FALSE),
      $this->getCreateExpectation(),
    ]);
    $item = $this->itemFinder->getItem($this->user, TRUE);
    $this->assertSame($this->updatedContact, $item);
    $this->assertItemManagerExpectationsMet();
  }

  /**
   * Test when an existing contact cannot be found, and creation is not allowed.
   */
  public function testFindNothing() {
    $this->itemManagerExpects([
      $this->getFindByEmailExpectation(FALSE),
    ]);
    $item = $this->itemFinder->getItem($this->user, FALSE);
    $this->assertNull($item);
    $this->assertItemManagerExpectationsMet();
  }

}
