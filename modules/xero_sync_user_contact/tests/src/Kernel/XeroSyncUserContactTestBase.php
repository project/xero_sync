<?php

namespace Drupal\Tests\xero_sync_user_contact\Kernel;

use Drupal\Tests\xero_sync\Kernel\QueuelessJobManager;
use Drupal\user\Entity\User;
use Drupal\Tests\xero_sync\Kernel\XeroSyncTestBase;
use Drupal\xero\TypedData\XeroComplexItemInterface;

/**
 * A base class for Xero Sync User Contact kernel tests.
 *
 * @group xero_sync
 */
abstract class XeroSyncUserContactTestBase extends XeroSyncTestBase {

  /**
   * Drupal user id / Xero Contact ContactNumber.
   *
   * @var int|string
   */
  protected $uid;

  /**
   * Drupal user username / Xero Contact Name.
   *
   * @var string
   */
  protected $username;

  /**
   * Drupal user mail / Xero Contact EmailAddress.
   *
   * @var string
   */
  protected $email;

  /**
   * Drupal user xero_sync / Xero Contact ContactID.
   *
   * @var string
   */
  protected $xeroId;

  /**
   * Drupal user xero_sync / Xero Contact ContactID.
   *
   * @var string
   */
  protected $unsyncXeroId;


  /**
   * Drupal user .
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * A typed data representation of a Xero contact.
   *
   * @var \Drupal\xero\TypedData\XeroComplexItemInterface
   */
  protected $foundContact;

  /**
   * A typed data representation of a Xero contact.
   *
   * @var \Drupal\xero\TypedData\XeroComplexItemInterface
   */
  protected $updatedContact;

  /**
   * A typed data representation of a Xero contact.
   *
   * @var \Drupal\xero\TypedData\XeroComplexItemInterface
   */
  protected $creatingContact;

  /**
   * A typed data representation of a Xero contact.
   *
   * @var \Drupal\xero\TypedData\XeroComplexItemInterface
   */
  protected $unsyncedContact;

  /**
   * A typed data representation of a stub Xero contact.
   *
   * @var \Drupal\xero\TypedData\XeroComplexItemInterface
   */
  protected $stubContact;

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('user');

    $this->xeroId = $this->createGuid(FALSE);
    $this->unsyncXeroId = $this->xeroId;
    $this->email = mb_strtolower($this->randomMachineName()) . '@example.com';
    $this->username = $this->randomMachineName();
    $this->uid = $this->generateRandomEntityId();

    $this->user = User::create([
      'mail' => $this->email,
      'name' => $this->username,
      'uid' => $this->uid,
    ]);
    // Because we set the uid, the user will think it is not new.
    $this->user->enforceIsNew(TRUE);
  }

  /**
   * Expect an attempt to find a contact by email.
   *
   * @param bool $success
   *   Whether the attempt is expected to be succesful.
   * @param \Drupal\xero\TypedData\XeroComplexItemInterface|null $foundContact
   *   The found item.
   *
   * @return array
   *   An array of expectations.
   */
  protected function getFindByEmailExpectation($success, XeroComplexItemInterface $foundContact = NULL) {
    if (is_null($foundContact)) {
      $foundContact = $this->getFoundContact();
    }
    $expectation = [
      'method' => 'findItem',
      'arguments' => [
        'xero_contact',
        [['EmailAddress.ToLower()', $this->email]],
      ],
      'return' => $foundContact,
    ];
    if (!$success) {
      $expectation['return'] = NULL;
    }
    return $expectation;
  }

  /**
   * Expect an attempt to update a contact.
   */
  protected function getUpdateExpectation() {
    return [
      'method' => 'updateItem',
      'arguments' => [
        $this->getUpdatedContact(),
      ],
      'return' => $this->getUpdatedContact(),
    ];
  }

  /**
   * Expect an attempt to create a contact.
   */
  protected function getCreateExpectation() {
    return [
      'method' => 'createItem',
      'arguments' => [
        $this->getCreatingContact(),
      ],
      'return' => $this->getUpdatedContact(),
    ];
  }

  /**
   * Expect an attempt to create a contact.
   */
  protected function getFailedCreateExpectation() {
    return [
      'method' => 'createItem',
      'arguments' => [
        $this->getCreatingContact(),
      ],
      'return' => FALSE,
    ];
  }

  /**
   * Expect an attempt to unsync a contact.
   */
  protected function getUnsyncExpectation() {
    return [
      'method' => 'updateItem',
      'arguments' => [
        $this->getUnsyncingContact(),
      ],
      'return' => $this->getFoundContact(),
    ];
  }

  /**
   * Get a stub contact.
   */
  protected function getStubContact() {
    $this->stubContact = $this->buildXeroItem(
      'xero_contact',
      [
        'ContactID' => $this->xeroId,
      ]);
    $this->stubContact->stub = TRUE;
    return $this->stubContact;
  }

  /**
   * Get the found contact.
   *
   * @return \Drupal\xero\TypedData\XeroComplexItemInterface
   *   The contact.
   */
  protected function getFoundContact() {
    $this->foundContact = $this->buildXeroItem(
      'xero_contact',
      [
        'ContactID' => $this->xeroId,
        'EmailAddress' => $this->email,
        'Name' => "{$this->username} ({$this->uid})",
      ]);
    $this->foundContact->stub = FALSE;
    return $this->foundContact;
  }

  /**
   * Get the updated contact.
   *
   * @return \Drupal\xero\TypedData\XeroComplexItemInterface
   *   The contact.
   */
  protected function getUpdatedContact() {
    $this->updatedContact = $this->buildXeroItem(
      'xero_contact',
      [
        'Name' => "{$this->username} ({$this->uid})",
        'ContactNumber' => $this->uid,
        'EmailAddress' => $this->email,
        'ContactID' => $this->xeroId,
      ]);
    $this->updatedContact->stub = FALSE;
    return $this->updatedContact;
  }

  /**
   * Get the contact to be created.
   *
   * @return \Drupal\xero\TypedData\XeroComplexItemInterface
   *   The contact.
   */
  protected function getCreatingContact() {
    $this->creatingContact = $this->buildXeroItem(
      'xero_contact',
      [
        'Name' => "{$this->username} ({$this->uid})",
        'ContactNumber' => $this->uid,
        'EmailAddress' => $this->email,
      ]);
    return $this->creatingContact;
  }

  /**
   * Get the contact to unsync.
   *
   * @return \Drupal\xero\TypedData\XeroComplexItemInterface
   *   The contact.
   */
  protected function getUnsyncingContact() {
    $this->unsyncingContact = $this->buildXeroItem(
      'xero_contact',
      [
        'ContactID' => $this->unsyncXeroId,
        'ContactNumber' => '',
      ]);
    return $this->unsyncingContact;
  }

  /**
   * Save a user and assert that expectations are then met.
   *
   * @param bool $referenced
   *   Whether to expect the user to store an item reference.
   * @param array $itemManagerExpectations
   *   The item manager expectations.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function assertUserSave($referenced, array $itemManagerExpectations) {
    $this->itemManagerExpects($itemManagerExpectations);
    $this->assertInstanceOf(QueuelessJobManager::class, \Drupal::service('xero_sync.job_manager'));
    $this->user->save();
    $this->assertItemManagerExpectationsMet();
    if ($referenced) {
      $this->assertItemReferencedInField($this->user, 'xero_contact', $this->xeroId);
    }
    else {
      $this->assertNoItemReferencedInField($this->user);
    }
  }

  /**
   * Save a user and assert that expectations are met.
   *
   * @param array $itemManagerExpectations
   *   Item manager expectations.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function assertSuccessfulInsert(array $itemManagerExpectations) {
    $referenced = $itemManagerExpectations !== [];
    $this->assertUserSave($referenced, $itemManagerExpectations);
  }

}
