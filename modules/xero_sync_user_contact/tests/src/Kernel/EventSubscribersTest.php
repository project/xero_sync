<?php

namespace Drupal\Tests\xero_sync_user_contact\Kernel;

use Drupal\Core\Entity\EntityInterface;
use Drupal\user\Entity\User;
use Drupal\xero\TypedData\XeroComplexItemInterface;
use Drupal\xero_sync\Event\XeroSyncEvents;
use Drupal\xero_sync\Event\XeroSyncPropertiesEvent;
use Drupal\xero_sync\XeroSyncEntityHandler;

/**
 * Test the Xero Sync User Contact event subscribers.
 *
 * @group xero_sync
 */
class EventSubscribersTest extends XeroSyncUserContactTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'user',
    'serialization',
    'xero',
    'xero_sync',
    'xero_sync_user_contact',
  ];

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->disableEntityHandler();
    $this->expectEntityUpdates(0);
    $this->eventDispatcher = \Drupal::service('event_dispatcher');
  }

  /**
   * Test the sync to contact even when the event modifies the contact.
   */
  public function testSyncToContactModified() {
    $contact = $this->buildXeroItem('xero_contact', [
      'ContactID' => $this->xeroId,
      'EmailAddress' => 'oldemail@example.com',
      'Name' => 'Old name',
    ]);
    $user = $this->createSyncedUser();
    $event = $this->dispatchEvent(XeroSyncEvents::SYNC_TO_ITEM_FROM_ENTITY, $user, $contact);
    $item = $event->getItem();

    $this->assertTrue($event->isModified());
    $this->assertEquals($this->uid, $item->get('ContactNumber')->getString());
    $this->assertEquals($this->email, $item->get('EmailAddress')->getString());
    $this->assertEquals("{$this->username} ({$this->uid})", $item->get('Name')->getString());
  }

  /**
   * Test the sync to contact event when not modified.
   */
  public function testSyncToContactNotModified() {
    $contact = $this->buildXeroItem('xero_contact', [
      'ContactID' => $this->xeroId,
      'ContactNumber' => $this->uid,
      'EmailAddress' => $this->email,
      'Name' => "{$this->username} ({$this->uid})",
    ]);
    $user = $this->createSyncedUser();
    $event = $this->dispatchEvent(XeroSyncEvents::SYNC_TO_ITEM_FROM_ENTITY, $user, $contact);
    $item = $event->getItem();

    $this->assertFalse($event->isModified(), "Event should not be modified.");
    $this->assertEquals($this->uid, $item->get('ContactNumber')->getString());
    $this->assertEquals($this->email, $item->get('EmailAddress')->getString());
    $this->assertEquals("{$this->username} ({$this->uid})", $item->get('Name')->getString());
  }

  /**
   * Test the sync to contact event with null email.
   */
  public function testSyncToContactNullEmail() {
    $contact = $this->buildXeroItem('xero_contact', [
      'ContactID' => $this->xeroId,
      'EmailAddress' => $this->email,
      'Name' => "{$this->username} ({$this->uid})",
    ]);
    $user = $this->createSyncedUser(['mail' => NULL]);
    $this->assertNull($user->getEmail());
    $event = $this->dispatchEvent(XeroSyncEvents::SYNC_TO_ITEM_FROM_ENTITY, $user, $contact);
    $item = $event->getItem();

    $this->assertEquals($this->email, $item->get('EmailAddress')->getString());
  }

  /**
   * Test the sync to user event.
   */
  public function testSyncToUser() {
    $contact = $this->buildXeroItem('xero_contact', [
      'ContactID' => $this->xeroId,
    ]);
    $user = $this->createSyncedUser();
    $event = $this->dispatchEvent(XeroSyncEvents::SYNC_TO_ENTITY_FROM_ITEM, $user, $contact);

    $this->assertTrue($event->isModified());

    $this->reloadEntity($user);
    $userXeroType = $user->get(XeroSyncEntityHandler::DEFAULT_FIELD_NAME)->first()->type;
    $userXeroId = $user->get(XeroSyncEntityHandler::DEFAULT_FIELD_NAME)->first()->guid;
    $this->assertEquals('xero_contact', $userXeroType);
    $this->assertEquals($this->xeroId, $userXeroId);
  }

  /**
   * Test the unsync event.
   */
  public function testUnsyncContact() {
    $contact = $this->buildXeroItem('xero_contact', [
      'ContactID' => $this->xeroId,
      'ContactNumber' => $this->uid,
    ]);
    $user = $this->createSyncedUser();
    $event = $this->dispatchEvent(XeroSyncEvents::UNSYNC_ITEM, $user, $contact);
    $item = $event->getItem();

    $this->assertTrue($event->isModified());
    $this->assertEquals(NULL, $item->get('ContactNumber')->getValue());
  }

  /**
   * Dispatch a sync event.
   *
   * @param string $eventType
   *   The event type.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param \Drupal\xero\TypedData\XeroComplexItemInterface $item
   *   The item.
   *
   * @return \Drupal\xero_sync\Event\XeroSyncPropertiesEvent
   *   The dispatched event, possibly modified by subscribers.
   */
  protected function dispatchEvent($eventType, EntityInterface $entity, XeroComplexItemInterface $item) {
    $event = new XeroSyncPropertiesEvent($entity, $item);
    $eventId = $eventType . "." . $entity->getEntityTypeId();
    $event = $this->eventDispatcher->dispatch($event, $eventId);
    return $event;
  }

  /**
   * Create a synced user.
   *
   * @param array $suppliedFields
   *   The fields to set on the user.
   *
   * @return \Drupal\Core\Entity\EntityInterface|\Drupal\user\Entity\User
   *   The user.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createSyncedUser(array $suppliedFields = []) {
    $defaultFields = [
      'uid' => $this->uid,
      'mail' => $this->email,
      'name' => $this->username,
    ];
    $fields = $suppliedFields + $defaultFields;
    $user = User::create($fields);
    $user->save();
    return $user;
  }

}
