<?php

namespace Drupal\Tests\xero_sync_user_contact\Kernel;

use Drupal\xero_sync\XeroSyncEntityHandler;

/**
 * Test the creation of a new user.
 *
 * @group xero_sync
 */
class QueuelessUserCreationTest extends XeroSyncUserContactTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'user',
    'serialization',
    'xero',
    'xero_sync',
    'xero_sync_user_contact',
  ];

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->mockItemManager();
    $this->bypassQueues();
  }

  /**
   * Test what happens when a user is created, and contact is found.
   *
   * If the contact has the same email but a different name, then the name
   * should be updated.
   */
  public function testInsertWithItemFoundByEmailButNameWrong() {
    $contact = $this->buildXeroItem(
      'xero_contact',
      [
        'ContactID' => $this->xeroId,
        'EmailAddress' => $this->email,
        'Name' => 'wrong name',
        'ContactNumber' => $this->uid,
      ]);

    $itemManagerExpectations = [
      $this->getFindByEmailExpectation(TRUE, $contact),
      $this->getUpdateExpectation(),
    ];

    $this->assertSuccessfulInsert($itemManagerExpectations);
  }

  /**
   * Test what happens when a user is created, and contact is found.
   *
   * If there is a contact with the same email, but without a contact number,
   * then the contact number should be set.
   */
  public function testInsertWithItemFoundByEmailMissingNumber() {
    $contact = $this->buildXeroItem(
      'xero_contact',
      [
        'ContactID' => $this->xeroId,
        'EmailAddress' => $this->email,
        'Name' => "{$this->username} ({$this->uid})",
      ]);

    $itemManagerExpectations = [
      $this->getFindByEmailExpectation(TRUE, $contact),
      $this->getUpdateExpectation(),
    ];

    $this->assertSuccessfulInsert($itemManagerExpectations);
  }

  /**
   * Test what happens when a user is created, and contact is found.
   *
   * If there is a contact with the same email that does not need updating,
   * it should not be updated.
   */
  public function testInsertWithItemFoundByEmailAllComplete() {
    $foundContact = $this->buildXeroItem(
      'xero_contact',
      [
        'ContactID' => $this->xeroId,
        'EmailAddress' => $this->email,
        'Name' => "{$this->username} ({$this->uid})",
        // Importantly and improbably the contact number is already
        // set on the found contact so no update is needed.
        'ContactNumber' => $this->uid,
      ]);

    $expectation = [
      'method' => 'findItem',
      'arguments' => [
        'xero_contact',
        [['EmailAddress.ToLower()', $this->email]],
      ],
      'return' => $foundContact,
    ];

    $itemManagerExpectations = [
      $expectation,
    ];

    $this->assertSuccessfulInsert($itemManagerExpectations);
  }

  /**
   * Test what happens when a user is created, and no contact is found.
   *
   * If there is not contact with the same email, a contact should be created.
   */
  public function testInsertWithNoItemFound() {
    $itemManagerExpectations = [
      $this->getFindByEmailExpectation(FALSE),
      $this->getCreateExpectation(),
    ];

    $this->assertSuccessfulInsert($itemManagerExpectations);
  }

  /**
   * Test what happens when a user is created with field filled.
   *
   * As there already is a Xero contact identified in the user's field, the
   * contact should be updated.
   */
  public function testInsertWithFieldFilled() {
    $itemManagerExpectations = [
      $this->getUpdateExpectation(),
    ];

    $this->user->set(XeroSyncEntityHandler::DEFAULT_FIELD_NAME, [
      'type' => 'xero_contact',
      'guid' => $this->xeroId,
    ]);

    $this->assertSuccessfulInsert($itemManagerExpectations);
  }

}
