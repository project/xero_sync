<?php

namespace Drupal\Tests\xero_sync_user_contact\Kernel;

use Drupal\xero_sync\XeroSyncEntityHandler;

/**
 * Test the updating of a user.
 *
 * @group xero_sync
 */
class QueuelessUserUpdatingTest extends XeroSyncUserContactTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'user',
    'serialization',
    'xero',
    'xero_sync',
    'xero_sync_user_contact',
  ];

  /**
   * {@inheritDoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->installSchema('user', ['users_data']);
    $this->mockItemManager();
    $this->bypassQueues();
    $this->setXeroField($this->user, 'xero_contact', $this->xeroId);
    $this->assertUserSave(TRUE, [
      // No find or create because item is already referenced in field.
      // Update needed to set ContactNumber on item.
      $this->getUpdateExpectation(),
    ]);
    // Clear the item finder's static cache, so that the update triggered by the
    // save above does not leave a cached result.
    \Drupal::service('plugin.manager.xero_sync_item_finder')->clearCache();
  }

  /**
   * Email changes should be sent to Xero for synced users.
   */
  public function testChangedEmail() {
    $this->email = $this->randomMachineName() . "new@example.com";
    $this->user->set('mail', $this->email);
    $this->assertUserSave(TRUE, [
      // No find or create because item is already referenced in field.
      // Update needed because email should be updated on item.
      $this->getUpdateExpectation(),
    ]);
  }

  /**
   * Name changes should be sent to Xero for synced users.
   */
  public function testChangedName() {
    $this->username = $this->randomMachineName();
    $this->user->set('name', $this->username);
    $this->assertUserSave(TRUE, [
      // No find or create because item is already referenced in field.
      // Update needed because name should be updated on item.
      $this->getUpdateExpectation(),
    ]);
  }

  /**
   * Timezone changes should not be synced to Xero.
   */
  public function testChangedTimezone() {
    $this->user->set('timezone', "Europe/Paris");
    // No find or create because item is already referenced in field.
    // No update needed as timezone is not a synced field.
    $this->assertUserSave(TRUE, []);
  }

  /**
   * Test what happens when a user's xero contact id is changed.
   */
  public function testChangedId() {
    $this->unsyncXeroId = $this->xeroId;
    $this->xeroId = $this->createGuid(FALSE);
    $this->setXeroField($this->user, 'xero_contact', $this->xeroId);
    $this->assertUserSave(TRUE, [
      // First unsync the previously referenced item.
      $this->getUnsyncExpectation(),
      // No find expectation because we already have the new reference item
      // Update needed to set the ContactNumber on the newly referenced item.
      $this->getUpdateExpectation(),
    ]);
  }

  /**
   * Test what happens when a user's xero contact id is removed.
   */
  public function testRemovedId() {
    $this->unsyncXeroId = $this->xeroId;
    $this->user->get(XeroSyncEntityHandler::DEFAULT_FIELD_NAME)->removeItem(0);
    $this->assertUserSave(FALSE, [
      // Unsync needed to remove the contact number from old item
      // No find because we don't assume that a find is immediately wanted.
      $this->getUnsyncExpectation(),
    ]);
  }

  /**
   * Test what happens when a synced user is deleted.
   */
  public function testSyncedDeletion() {
    $this->itemManagerExpects([$this->getUnsyncExpectation()]);
    $this->user->delete();
    $this->assertItemManagerExpectationsMet();
  }

  /**
   * Test what happens when an unsycned user is deleted.
   */
  public function testUnsyncedDeletion() {
    $this->testRemovedId();
    $this->itemManagerExpects([]);
    $this->user->delete();
    $this->assertItemManagerExpectationsMet();
  }

}
