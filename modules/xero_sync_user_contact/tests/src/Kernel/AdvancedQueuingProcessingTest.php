<?php

namespace Drupal\Tests\xero_sync_user_contact\Kernel;

use Drupal\advancedqueue\Entity\Queue;
use Drupal\advancedqueue\Job;
use Drupal\xero\XeroNullClient;
use Drupal\xero_sync\Plugin\AdvancedQueue\JobType\JobBase;
use Drupal\Core\Logger\RfcLogLevel;

/**
 * Test processing queue items with AdvancedQueue.
 *
 * @requires module advancedqueue
 * @group xero_sync
 */
class AdvancedQueuingProcessingTest extends QueuingProcessingTest {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'user',
    'serialization',
    'xero',
    'xero_sync',
    'xero_sync_user_contact',
    'advancedqueue',
    'views',
  ];

  /**
   * {@inheritDoc}
   */
  protected $jobClass = JobBase::class;

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // Disabling entity handler might not be needed here.
    $this->disableEntityHandler();
    $this->installSchema('advancedqueue', ['advancedqueue']);
    $data = [
      'id' => 'xero_sync_sync',
      'label' => 'Xero Sync sync',
      'backend' => 'database',
      'backend_configuration' => ['lease_time' => 60],
      'processor' => 'cron',
      'processing_time' => 60,
      'locked' => TRUE,
    ];
    $queue = Queue::create($data);
    $queue->save();
  }

  /**
   * Test the API rate limit being exceeded when finding.
   */
  public function testMinuteRateLimitExceededWhenFinding() {
    $this->logger->expects($this->at(0))
      ->method('log')
      ->with(
        RfcLogLevel::WARNING,
        $this->stringContains('Xero rate limit exceeded while processing xero_sync_sync queue, retry in 5 seconds')
      );
    $this->logger->expects($this->at(1))
      ->method('log')
      ->with(
        RfcLogLevel::INFO,
        $this->stringContains('Success in queue')
      );

    $this->xeroClient->expects($this->at(0))
      ->method('__call')
      ->with('get')
      ->willThrowException($this->getRequestException(429, '5'));

    // The job should be retried after a 5 second delay.
    $response = $this->getMockResponseFor("{$this->username} ({$this->uid})", $this->email, $this->uid);
    $this->xeroClient->expects($this->at(1))
      ->method('__call')
      ->with('get')
      ->willReturn($response);

    $this->expectEntityUpdates(1);
    $this->user->save();

    $this->enqueueJob();
    $this->runQueueJob();

    // Running the queue again immediately will have no effect, as the retry
    // delay will not have been satisifed on the stored queue job and the
    // processor will not pick it up.
    $this->runQueueJob();
    sleep(5);

    $this->runQueueJob();
    $this->assertItemReferencedInField($this->user, 'xero_contact', $this->xeroId);
  }

  /**
   * Test throwing an exception when creating.
   */
  public function testInvalidClientWhenCreating() {
    $this->logger->expects($this->once())
      ->method('log')
      ->with(
        RfcLogLevel::ERROR,
        $this->stringContains('Exception in queue')
      );
    $response = $this->getMockResponseForEmpty();
    $this->mockFindByEmail($response);
    $this->xeroClient = $this->createMock(XeroNullClient::class);
    $this->assertUserCreation(FALSE);
  }

  /**
   * {@inheritDoc}
   */
  protected function enqueueJob() {
    /** @var \Drupal\advancedqueue\Entity\QueueInterface $queue */
    $queue = Queue::load('xero_sync_sync');

    $jobCounts = $queue->getBackend()->countJobs();
    foreach ($jobCounts as $jobCount) {
      $this->assertEquals(0, $jobCount);
    }
    $queue->getBackend()->enqueueJob(Job::create('xero_sync_sync',
    [
      'entity_id' => $this->uid,
      'entity_type' => 'user',
    ]));
  }

  /**
   * {@inheritDoc}
   */
  protected function runQueueJob($repeat = 1) {
    // $repeat is ignored for AQ.
    /** @var \Drupal\advancedqueue\Entity\QueueInterface $queue */
    $queue = Queue::load('xero_sync_sync');
    /** @var \Drupal\advancedqueue\ProcessorInterface $queue_processor */
    $queue_processor = \Drupal::service('advancedqueue.processor');
    $queue_processor->processQueue($queue);
  }

  /**
   * Test the advanced queue report.
   *
   * Currently we don't use this, it would need a functional test.
   *
   * @param string $text1
   *   Text.
   * @param string $text2
   *   Text.
   */
  protected function assertAdvancedQueueReport($text1, $text2) {
    $this->drupalLogin($this->rootUser);
    $this->drupalGet('/admin/config/system/queues/jobs/xero_sync_sync');
    $this->assertSession()->responseContains($text1);
    $this->assertSession()->responseContains($text2);
  }

}
