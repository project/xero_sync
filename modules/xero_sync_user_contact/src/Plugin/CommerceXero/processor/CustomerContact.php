<?php

namespace Drupal\xero_sync_user_contact\Plugin\CommerceXero\processor;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_xero\Entity\CommerceXeroStrategyInterface;
use Drupal\commerce_xero\Plugin\CommerceXero\CommerceXeroProcessorPluginBase;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\TypedData\ComplexDataInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Sets Xero contact based on Xero contact synced with order customer.
 *
 * Uses Xero Sync's synchronizer mechanism for identifying or creating a synced
 * contact.
 *
 * In some checkouts flows users may be attached to the order only at the end.
 * This is why this is a 'process' processor not an 'immediate'
 * one, because the customer may not yet be attached to the order immediately
 * when the payment is received in middle of checkout flow.
 *
 * @CommerceXeroProcessor(
 *   id = "xero_sync_user_contact_customer",
 *   label = @Translation("Set Xero contact from Drupal Commerce order customer"),
 *   types = {},
 *   execution = "process",
 *   required = FALSE
 * )
 */
class CustomerContact extends CommerceXeroProcessorPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The Xero Sync synchronizer service.
   *
   * @var \Drupal\xero_sync\XeroSyncSynchronizerInterface
   */
  protected $synchronizer;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    // This should be parent::create(). But the base class has no create().
    $instance = new static(
      $configuration,
      $plugin_id,
      $plugin_definition);
    $instance->synchronizer = $container->get('xero_sync.synchronizer');
    $instance->logger = $container->get('logger.channel.xero_sync');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    return [];
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function process(PaymentInterface $payment, ComplexDataInterface &$data, CommerceXeroStrategyInterface $strategy = NULL): bool {
    // Replace the default contact with one synced with the customer.
    if ($order = $payment->getOrder()) {
      $customer = $payment->getOrder()->getCustomer();
      if ($customer && !$customer->isAnonymous()) {
        try {
          // Updating an existing contact here is not optimal as it's an extra
          // remote API call and we could theoretically do it when setting the
          // contact on the invoice.
          $contact = $this->synchronizer->syncEntityWithItem($customer, NULL, TRUE, TRUE);
        }
        catch (\Throwable $e) {
          $message = (string) new FormattableMarkup("Error synchronizing user @name (@uid) with Xero contact in commerce xero process queue \nError message: @message \nError trace:\n@trace", [
            '@uid' => $customer->id(),
            '@name' => $customer->getDisplayName(),
            '@message' => $e->getMessage(),
            '@trace' => $e->getTraceAsString(),
          ]);
          $this->logger->error($message);
        }
        if ($contact) {
          $data->set('Contact', [
            'ContactID' => $contact->get('ContactID')
              ->getString(),
          ]);
        }
      }
      return TRUE;
    }
  }

}
