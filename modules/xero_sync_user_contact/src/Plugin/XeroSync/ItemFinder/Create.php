<?php

namespace Drupal\xero_sync_user_contact\Plugin\XeroSync\ItemFinder;

use Drupal\xero_sync\Plugin\XeroSync\ItemFinder\ItemFinderBase;

/**
 * Provides an item finder plugin for users that creates Xero contact.
 *
 * This is the fallback plugin that creates contacts if none can be found by
 * other plugins with a higher priority.
 *
 * @XeroSyncItemFinder(
 *   id = "xero_sync_user_contact_create",
 *   priority = 0,
 *   create = TRUE,
 *   entity_types = {
 *     "user"
 *   }
 * )
 */
class Create extends ItemFinderBase {

  /**
   * {@inheritdoc}
   */
  public function getItem() {
    return $this->createItem('xero_contact');
  }

}
