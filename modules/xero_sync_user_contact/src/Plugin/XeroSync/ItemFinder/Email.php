<?php

namespace Drupal\xero_sync_user_contact\Plugin\XeroSync\ItemFinder;

use Drupal\xero_sync\Plugin\XeroSync\ItemFinder\ItemFinderBase;

/**
 * An item finder plugin that looks for a Xero contact matching a user's email.
 *
 * @XeroSyncItemFinder(
 *   id = "xero_sync_user_contact_email",
 *   priority = 50,
 *   create = FALSE,
 *   entity_types = {
 *     "user"
 *   }
 * )
 */
class Email extends ItemFinderBase {

  /**
   * {@inheritdoc}
   */
  public function getItem() {
    $entity = $this->getEntity();
    if ($entity->getEmail()) {
      $item = $this->findItem(
        'xero_contact',
        [
          [
            'EmailAddress.ToLower()',
            mb_strtolower($entity->getEmail()),
          ],
        ]
        );
      return $item;
    }
  }

}
