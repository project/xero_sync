<?php

namespace Drupal\xero_sync_user_contact\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\xero_sync\Event\XeroSyncEvents;
use Drupal\xero_sync\Event\XeroSyncPropertiesEvent;
use Drupal\user\UserInterface;

/**
 * Specify information that should be cleared from the Xero contact on unsync.
 */
class UnsyncContactSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      XeroSyncEvents::UNSYNC_ITEM . '.user' => 'clearContact',
    ];

    return $events;
  }

  /**
   * Specify information that should be cleared from the Xero contact on unsync.
   *
   * @param \Drupal\xero_sync\Event\XeroSyncPropertiesEvent $event
   *   The Xero Sync event.
   */
  public function clearContact(XeroSyncPropertiesEvent $event) {
    // This is just paranoia, as we're subscribing to a user event we
    // should be able to trust there's a user entity.
    $hasUser = $event->getEntityType() === 'user' && ($event->getEntity() instanceof UserInterface);
    $hasContact = $event->getItemType() === 'xero_contact';
    if ($hasUser && $hasContact) {
      $userId = (string) $event->getEntity()->id();
      $contactNumber = $event->getItem()->get('ContactNumber')->getString();
      // 'stub' indicates that the item we have is not the full item loaded from
      // Xero, i.e. whether we really know the current state of the remote item.
      $isStub = isset($event->getItem()->stub) && $event->getItem()->stub === TRUE;
      // If we don't know the details of the contact on Xero,
      // or we do know the details and the 'ContactNumber' property is set to
      // the user id, then blank it.
      if ($isStub || ($userId == $contactNumber)) {
        $event->setItemProperty('ContactNumber', NULL);
      }
    }
  }

}
