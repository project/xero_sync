<?php

namespace Drupal\xero_sync_user_contact\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\xero_sync\Event\XeroSyncEvents;
use Drupal\xero_sync\Event\XeroSyncPropertiesEvent;
use Drupal\user\UserInterface;

/**
 * Specify information that should be set on the Xero contact.
 */
class SyncToContactSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      XeroSyncEvents::SYNC_TO_ITEM_FROM_ENTITY . '.user' => 'specifyContact',
    ];

    return $events;
  }

  /**
   * Specify information that should be set on the Xero contact.
   *
   * @param \Drupal\xero_sync\Event\XeroSyncPropertiesEvent $event
   *   The Xero Sync event.
   */
  public function specifyContact(XeroSyncPropertiesEvent $event) {
    // This is just paranoia, as we're subscribing to a user event we
    // should be able to trust there's a user entity.
    $hasUser = $event->getEntityType() === 'user' && ($event->getEntity() instanceof UserInterface);
    $hasContact = $event->getItemType() === 'xero_contact';
    if ($hasUser && $hasContact) {
      $this->specifyName($event);
      $this->specifyContactNumber($event);
      $this->specifyEmailAddress($event);
    }
  }

  /**
   * Specify the Name property of the Xero contact item.
   *
   * @param \Drupal\xero_contact_sync\Event\XeroSyncPropertiesEvent $event
   *   The event.
   */
  protected function specifyName(XeroSyncPropertiesEvent $event) {
    // Name is required by Xero, but must be unique.
    // Any sensible user object must have a display name and id.
    $user = $event->getEntity();
    $displayName = $user->getDisplayName();
    $id = $user->id();
    $event->setItemProperty('Name', "$displayName ($id)");
  }

  /**
   * Specify the ContactNumber property of the Xero contact item.
   *
   * @param \Drupal\xero_contact_sync\Event\XeroSyncPropertiesEvent $event
   *   The event.
   */
  protected function specifyContactNumber(XeroSyncPropertiesEvent $event) {
    // User id can be empty if user is new or account is anonymous.
    if (!empty($event->getEntity()->id())) {
      $event->setItemProperty('ContactNumber', $event->getEntity()->id());
    }
  }

  /**
   * Specify the ContactNumber property of the Xero contact item.
   *
   * @param \Drupal\xero_contact_sync\Event\XeroSyncPropertiesEvent $event
   *   The event.
   */
  protected function specifyEmailAddress(XeroSyncPropertiesEvent $event) {
    // User email doesn't have to be required.
    if (!empty($event->getEntity()->getEmail())) {
      $event->setItemProperty('EmailAddress', $event->getEntity()->getEmail());
    }
  }

}
