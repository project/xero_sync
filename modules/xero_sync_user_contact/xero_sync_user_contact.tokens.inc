<?php

/**
 * @file
 * Provide a token for a synced contact's guid.
 */

use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_token_info().
 */
function xero_sync_user_contact_token_info() {
  $user['xero_sync_contact_id'] = [
    'name' => t("Xero Sync Contact ID"),
    'description' => t('The ContactID of the Xero Contact synced with the user'),
  ];

  return [
    'tokens' => ['user' => $user],
  ];
}

/**
 * Implements hook_tokens().
 */
function xero_sync_user_contact_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];

  if ($type == 'user' && !empty($data['user'])) {
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'xero_sync_contact_id':
          $replacements[$original] = $data['user']->get('xero_sync')->guid;
          break;
      }
    }
  }
  return $replacements;
}
