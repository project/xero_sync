<?php

/**
 * @file
 * Xero Sync api.
 *
 * Hooks provided by the Xero Sync module.
 */

/**
 * Alter ItemFinder plugin definitions.
 *
 * @param array $definitions
 *   An array of ItemFinder plugin definitions.
 *
 * @see \Drupal\xero_sync\ItemFinder
 * @see \Drupal\xero_sync\Plugin\XeroSync\ItemFinder\ItemFinderBase
 * @see \Drupal\xero_sync\Annotation\XeroSyncItemFinder
 */
function hook_xero_sync_item_finder_plugin_alter(array &$definitions) {
  // Never create contacts for users.
  if (isset($definitions['xero_sync_user_contact_create'])) {
    unset($definitions['xero_sync_user_contact_create']);
  }
}
